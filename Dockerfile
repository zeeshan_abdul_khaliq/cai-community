FROM node:14-alpine as builder
COPY package.json package-lock.json ./
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app
WORKDIR /ng-app
COPY . .
#RUN node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/ng build optimizer=false --aot --sou--configuration=localstage --build-rce-map=false --output-hashing none
RUN npm run ng build --aot --localstage --build-rce-map=false --max_old_space_size=8192
FROM nginx:stable-alpine
RUN mkdir -p /usr/share/nginx/html/auth/dist 
RUN apk add nano iputils bash
COPY --from=builder /ng-app/dist /usr/share/nginx/html/auth/dist/
RUN addgroup -g 1000 -S www-data \
&& adduser -u 1000 -D -S -G www-data www-data ||:
ADD nginx.conf /etc/nginx/
RUN chmod 775 -R /usr/share/nginx/html
ADD auth.conf /etc/nginx/conf.d/
EXPOSE 80:80