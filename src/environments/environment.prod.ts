/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  // API_URL: "https://192.168.18.27:443/api/",
  // Search_URL: "https://192.168.18.27:443/api/",
  API_URL: 'https://tip-stage.cydea.tech:2096/api/',
  Search_URL: 'https://tip-stage.cydea.tech:2096/api/',
  // DevOps can change the values here
  Stix_URL: 'http://3.133.242.105:5051/feedsip1/collections/91a7b528-80eb-42ed-a74d-c6fbd5a26116/objects/',
  Password: 'xxxxx',
  STIX_Url_Feeds: 'http://58.65.161.138:5053',
};
