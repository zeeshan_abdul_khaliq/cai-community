/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  API_URL: 'https://tip-stage.cydea.tech:2096/api/',
  Search_URL: 'https://api-stage.cydea.tech:443/api/',
  // DevOps can change the values here
  Stix_URL: 'https://tip-stage.cydea.tech:2053/feedsip1/collections/91a7b528-80eb-42ed-a74d-c6fbd5a26116/objects/',
  Password: 'admin@TRIAM_TI',
  STIX_Url_Feeds: 'https://tip-stage.cydea.tech:2083'
};
