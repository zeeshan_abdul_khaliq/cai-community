import {Injectable, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
@Injectable()
export class ConfigApiService {
    baseUrl: string;
    stixUrlFeeds: string;

    constructor() {
        this.baseUrl = environment.API_URL;
        this.stixUrlFeeds = environment.STIX_Url_Feeds;
    }
    /*-----------------------Adding to Tip Services------------------------------------------*/

    communityIndicator(value, type, category): string { // The function would find the indicator specified by the value
        return this.baseUrl + 'indicator?value=' + value + '&type=' + type + '&category=' + category;
    }

    communityIndicatorCompletevalueType(value, type): string {
        return this.baseUrl + 'indicator?value=' + value + '&type=' + type;
    }

    communityIndiactoronlyValue(value): string {
        return this.baseUrl + '/indicator?value=' + value;
    }

    communityDashboardheaderCounts(date, from, to): string {
        if (!date) {
            return this.baseUrl + 'indicator/total-count?date=false';
        } else  {
            return this.baseUrl + 'indicator/total-count?date=true&dateFrom=' + from + '&dateTo=' + to;
        }

    }

    comunitytotalCountIOCDetails(type, date, from, to): string {
        if (date === false) {
            return this.baseUrl + 'indicator/total-count-details?type=' + type + '&date=false';
        } else if (date === true) {
            return this.baseUrl + 'indicator/total-count-details?type=' + type + '&date=true&dateFrom=' + from + '&dateTo=' + to;
        }
    }

    communityweeklyStats(type): string {
        return this.baseUrl + 'indicator/weekly-stats?type=' + type;
    }
    communityContributorProfile(username): string {
        return  this.baseUrl + 'indicator/users-detail?username=' + username;
    }

    downloadCSV(id): string {
        return this.baseUrl + 'community/collections/download-indicators?collectionId=' + id;
    }

    communityTopContributors(): string {
        return this.baseUrl + 'community/users/top-contributors';
    }

    communityIndicatorDelApprove(): string {
        return this.baseUrl + 'indicator/del-approve';
    }

    communityIndicatorMAlware(type, category, description, userId, tags): string {
        return this.baseUrl + 'indicator/malware?type=' + type + '&category=' + category + '&description=' + description +
            '&emailId=' + userId + '&tags=' + tags;
    }

    communityIndicatorTAGS(): string {
        return this.baseUrl + 'indicator/tags';
    }

    communityIndicatorTopTAGS(): string {
        return this.baseUrl + 'community/tags';
    }

    communityIndicatorTOP(type): string {
        return this.baseUrl + 'indicator/top?type=' + type;
    }

    communityIndicatorTOPuploaded(user): string {
        return this.baseUrl + 'indicator/top-uploaded?user=' + user;
    }

    communityIndicatorUSERDetail(useriD): string {
        return this.baseUrl + 'indicator/user-detail?userId=' + useriD;
    }

    communityIndicatorUTOPSTATS(): string {
        return this.baseUrl + 'community/indicator/user-stats';
    }

    communityIndicatorUSERStats(useriD): string {
        return this.baseUrl + 'indicator/user-stats?userId=' + useriD;
    }

    communityIndicatorUsesrsTOP(type): string {
        return this.baseUrl + 'indicator/user/top?type=' + type;
    }

    communityIndicatorGetByID(userID): string {
        return this.baseUrl + 'indicator/' + userID;
    }

    communityindicatorsALLindicators(counter, type, tag): string {
        const username = sessionStorage.getItem('username');
        if (type === 'Collections' && tag) {
            return this.baseUrl + 'community/collections/list?page=0&size=' + counter + '&tag=' + tag;
        } else if (type === 'Collections' && !tag) {
            return this.baseUrl + 'community/collections/list?page=' + counter + '&size=10&email=' + username;
        } else {
            return this.baseUrl + 'community/explore/list-indicators?type=' + type + '&page=' + counter + '&size=10&email=' + username;
        }
    }

    communityTagList(): string {
        const username = sessionStorage.getItem('username');
        return this.baseUrl + 'community/tags/tags-by-user-email?email=' + username;
    }

    communityCollectionsByTag(counter, tag): string {
        return this.baseUrl + 'community/collections/list?page=0&size=' + counter + '&tag=' + tag;
    }

    communityAdvisoryByTag(tag): string {
        return this.baseUrl + 'community/advisory/find-all?tag=' + tag;
    }

    communityGetIndicatorBytype(type): string {
        return this.baseUrl + 'indicator/get-indicators?type=' + type;
    }
    communitygetIndicatorsByTypeTag(type, tag): string {
        return this.baseUrl + 'indicator/get-indicators?type=' + type + '&tag=' + tag;
    }

    communitySearchgetIndicator(type, value): string {
        return this.baseUrl + 'indicator/get-indicators?type=' + type + '&value=' + value + '&history=false';
    }

    // -----------//
    // Community RVAMP
    // ----------//
    communityIndicatorPost(): string {
        return this.baseUrl + 'community/collections';
    }

    communityIndicatorPut(): string {
        return this.baseUrl + 'community/collections';
    }

    communityIndicatorRecentCollections(): string {
        return this.baseUrl + 'community/collections/recent-collections';
    }

    communityGetIndicatorDetails(useriD): string {
        return this.baseUrl + 'community/collections?id=' + useriD;
    }

    communityGetIndDetails(useriD): string {
        return this.baseUrl + 'community/indicators/indicator-details?page=0&size=200&workspace=false&value=' + useriD;
    }

    // malware-upload?collection=
    communityIndicatorIOC(): string {
        return  this.baseUrl + 'community/collections/file-upload';
    }

    communityWorkSpaceIndicatorDetails(counter, value): string {
        return this.baseUrl + 'community/indicator-details?page=0&size=' + counter + '&workspace=true&value=' + value;
    }

    communityIndiactorExploreCount(): string {
        return  this.baseUrl + 'community/collections/total-count-details';
    }

    communityIndiactorContributorCount(email, boolValue): string {
        if (boolValue) {
            return this.baseUrl + 'community/collections/total-count-details?displayName=' + email;
        } else {
            return this.baseUrl + 'community/collections/total-count-details?email=' + email;
        }
    }

    communityIndiactorExploreHashCount(): string {
        return this.baseUrl + 'community/collections/hash-count-details';
    }

    communitygetIndicatorsByTypeCollections(counter, tag): string {
        if (tag) {
            return this.baseUrl + 'community/collections/list?page=' + counter + '&size=10&tag=' + tag;
        } else {
            return this.baseUrl + 'community/collections/list?page=' + counter + '&size=10';
        }
    }

    communitygetIndicatorsByType(type, counter): string {
        if (type === 'Collections') {
        } else if (type === 'Contributors') {
        } else {
            return this.baseUrl + 'community/explore/list-indicators?type=' + type + '&page=' +
                counter + '&size=10';
        }
    }

    communitygetIndicatorMalware(type): string {
        return this.baseUrl + 'community/collections/malware-download?name=' + type;
    }

    communitygetIndicatorsByContributors(counter): string {
        if (counter !== null) {
            return this.baseUrl + 'community/users/contributor-detail?page=' + counter + '&size=10';
        } else {
            return this.baseUrl + 'community/users/contributor-detail?page=' + counter + '&size=10';
        }
    }

    communitygetTAGdetails(tag): string {
        return this.baseUrl + 'community/tags/tag-counts?value=' + tag;
    }

    communityAdvisoryPost(): string {
        return this.baseUrl + 'community/advisory';
    }

    communityAdvisotyFindALL(tag): string {
        if (tag) {
            return this.baseUrl + 'community/advisory/find-all?tag=' + tag;
        } else {
            return this.baseUrl + 'community/advisory/find-all';
        }
    }

    communityAdvisotyDelete(id): string {
        return this.baseUrl + 'community/advisory?id=' + id;
    }

    communityAdvisoryExpire(id): string {
        return this.baseUrl + 'community/advisory/expire?id=' + id;
    }

    updateAdvisoryGetAll(id): string {
        return this.baseUrl + 'community/advisory?id=' + id;
    }

    communityAdvisoryEditPost(): string {
        return this.baseUrl + 'community/advisory';
    }

    communityIndicatorDelete(userID): string {
        return this.baseUrl + 'community/collections?id=' + userID;
    }

    // malware-upload?collection=
    communityIndicatorMalware(jsonVal): string {
        return this.baseUrl + 'community/collections?collection=' + jsonVal;
    }

    communityGetIndicatorBytypeContributor(type, Id, size): string {
        if (type === 'Collections') {
            return this.baseUrl + 'community/collections/list?page=0&size=' + size + '&email=' + Id;
        } else {
            return this.baseUrl + 'community/explore/list-indicators?type=' + type + '&email=' + Id + '&page=0&size=' + size ;
        }
    }

    // search
    SearchForContrubutors(counter, contributor): string {
        return this.baseUrl + 'community/collections/list?page=0&size=' + counter + '&displayName=' + contributor;
    }

    saearchForCollection(counter, collection): string {
        return this.baseUrl + 'community/collections/title?page=0&size=' + counter + '&title=' + collection;
    }

    searchForIndicator(counter, indicator): string {
        return this.baseUrl + 'community/indicator-details?page=0&size=' + counter + '&workspace=false&value=' + indicator;
    }

    advisoryOntickeer(): string {
        return this.baseUrl + 'community/advisory/find-all-ticker';
    }

}
