import {Injectable, OnInit} from '@angular/core';
import {ConfigApiService} from './configApi.service';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
    responseType: 'arraybuffer' as 'arraybuffer',
};
const httpOptionss = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
};
const httpOptionsss = {
    headers: new HttpHeaders({'Content-Type': 'text/plain'}),
};
@Injectable()
export class CommunityService {
    req: any;
    success: any;
    private historySub = new Subject<boolean>();
    private storageSub = new Subject<boolean>();
    private storageHeader = new Subject<boolean>();
    constructor(private http: HttpClient, private configApi: ConfigApiService) {
    }
    watchStorageHeader(): Observable<any> {
        return this.storageHeader.asObservable();
    }

    saveitemHeader(data): any {
        this.storageHeader.next(true);
    }
    watchHistory(): Observable<any> {
        return this.historySub.asObservable();
    }



    savehistory(): any {
        this.historySub.next(false);

    }
    watchStorage(): Observable<any> {
        return this.storageSub.asObservable();
    }
    exploreUpdate(): any {
        this.storageSub.next(true);
    }
    communityDashboardheaderCounts(date, from, to): any {
        const url = this.configApi.communityDashboardheaderCounts(date, from, to);
        if (url) {
            return this.http.get(url);
        }
    }

    comunitytotalCountIOCDetails(type, date, from, to): any {
        const url = this.configApi.comunitytotalCountIOCDetails(type, date, from, to);
        if (url) {
            return this.http.get(url);
        }
    }

    communityweeklyStats(type): any {
        const url = this.configApi.communityweeklyStats(type);
        return this.http.get(url);
    }
    downloadCSV(contId): any {
        const options = {responseType: 'blob' as 'json'};
        const url = this.configApi.downloadCSV(contId);
        return this.http.get<Blob>(url, options);
    }
    communityIndicatorCompletevalueType(value, type): any {
        const url = this.configApi.communityIndicatorCompletevalueType(value, type);
        return this.http.get(url);
    }

    communityIndicatorMAlware(file, type, category, description, userId, tags): any {
        const url = this.configApi.communityIndicatorMAlware(type, category, description, userId, tags);
        return this.http.post(url, file);
    }

    communityindicatorsALLindicators(counter, type, tag): any {
        const url = this.configApi.communityindicatorsALLindicators(counter, type, tag);
        return this.http.get(url);
    }

    communityTagList(): any {
        const url = this.configApi.communityTagList();
        return this.http.get(url);
    }

    communityCollectionsByTag(counter, tag): any {
        const url = this.configApi.communityCollectionsByTag(counter, tag);
        return this.http.get(url);
    }
    communityAdvisoryByTag(tag): any {
        const url = this.configApi.communityAdvisoryByTag( tag);
        return this.http.get(url);
    }
    communityWorkSpaceIndicatorDetails(counter, value): any {
        const url = this.configApi.communityWorkSpaceIndicatorDetails(counter, value);
        return this.http.get(url);
    }

    communityIndicatorDelete(userID): any {
        const url = this.configApi.communityIndicatorDelete(userID);
        return this.http.delete(url);
    }

    communityIndiactoronlyValue(value): any {
        const url = this.configApi.communityIndiactoronlyValue(value);
        return this.http.get(url);
    }communityContributorProfile(username): any {
        const url = this.configApi.communityContributorProfile(username);
        return this.http.get(url);
    }

    communityGetIndicatorBytype(type): any {
        const url = this.configApi.communityGetIndicatorBytype(type);
        return this.http.get(url);
    }
    communitygetIndicatorsByTypeTag(type, tag): any {
        const url = this.configApi.communitygetIndicatorsByTypeTag(type, tag);
        return this.http.get(url);
    }

    communityIndicatorTopTAGS(): any {
        const url = this.configApi.communityIndicatorTopTAGS();
        return this.http.get(url);
    }

    communityTopContributors(): any {
        const url = this.configApi.communityTopContributors();
        return this.http.get(url);
    }

    communityIndicatorUTOPSTATS(): any {
        const url = this.configApi.communityIndicatorUTOPSTATS();
        return this.http.get(url);
    }

    communityIndicatorRecentCollections(): any {
        const url = this.configApi.communityIndicatorRecentCollections();
        return this.http.get(url);
    }

    // --------------POST FOR Upload ------------
    public uploadFile(files: Set<File>, event): any {

        // this will be the our resulting map
        files.forEach(file => {

            const formData: FormData = new FormData();
            formData.append('file', file, file.name);
            if (event === null) {
                this.req = new HttpRequest('POST',
                    this.configApi.communityIndicatorIOC(),
                    formData, {});
            } else {
                this.req = new HttpRequest('POST',
                    this.configApi.communityIndicatorMalware(encodeURIComponent(JSON.stringify(event))),
                    formData, {});
            }
            try {
                this.http.request(this.req).subscribe(res => {
                    this.success = res;
                }, error => {});
            } catch (e) {
            }
        });
    }

    communityIndicatorIOC(uploadedFile, jsonVal): any {
        const url = this.configApi.communityIndicatorIOC();
        return this.http.post(url, uploadedFile);
    }

    communityIndicatorMalware(file, jsonVal): any {
        const url = this.configApi.communityIndicatorMalware(encodeURIComponent(JSON.stringify(jsonVal)));
        return this.http.post(url, file);
    }

    communityIndicatorPost(data): any {
        const url = this.configApi.communityIndicatorPost();
        return this.http.post(url, data);
    }

    communityIndicatorPut(data): any {
        const url = this.configApi.communityIndicatorPost();
        return this.http.put(url, data);
    }

    communityIndiactorExploreCount(): any {
        const url = this.configApi.communityIndiactorExploreCount();
        return this.http.get(url);
    }

    communityIndiactorContributorCount(email, boolValue): any {
        const url = this.configApi.communityIndiactorContributorCount(email, boolValue);
        return this.http.get(url);
    }

    communityIndiactorExploreHashCount(): any {
        const url = this.configApi.communityIndiactorExploreHashCount();
        return this.http.get(url);
    }


    communitygetIndicatorsByTypeCollections(counter, tag): any {
        const url = this.configApi.communitygetIndicatorsByTypeCollections(counter, tag);
        return this.http.get(url);
    }

    communitygetIndicatorsByType(type, counter): any {
        const url = this.configApi.communitygetIndicatorsByType(type, counter);
        if (url) {
            return this.http.get(url);
        }
    }

    communitygetIndicatorsMalware(type): any {
        const options = {responseType: 'blob' as 'json'};
        const url = this.configApi.communitygetIndicatorMalware(type);
        return this.http.get(url, options);
    }

    communitygetIndicatorsByContributors(counter): any {
        const url = this.configApi.communitygetIndicatorsByContributors(counter);
        return this.http.get(url);
    }

    communitygetTAGdetails(tag): any {
        const url = this.configApi.communitygetTAGdetails(tag);
        return this.http.get(url);
    }

    communityGetIndicatorDetails(type): any {
        const url = this.configApi.communityGetIndicatorDetails(type);
        return this.http.get(url);
    }

    communityGetIndDetails(type): any {
        const url = this.configApi.communityGetIndDetails(type);
        return this.http.get(url);
    }

    communityGetIndicatorBytypeContributor(type, Id, size): any {
        const url = this.configApi.communityGetIndicatorBytypeContributor(type, Id, size);
        return this.http.get(url);
    }

    communityAdvisoryPost(data): any {
        const url = this.configApi.communityAdvisoryPost();
        return this.http.post(url, data);
    }

    communityAdvisotyFindALL(tag): any {
        const url = this.configApi.communityAdvisotyFindALL(tag);
        return this.http.get(url);
    }

    communityAdvisotyDelete(id): any {
        const url = this.configApi.communityAdvisotyDelete(id);
        return this.http.delete(url);
    }

    communityAdvisoryExpire(id): any {
        const url = this.configApi.communityAdvisoryExpire(id);
        return this.http.put(url, null);
    }

    updateAdvisoryGetAll(id): any {
        const url = this.configApi.updateAdvisoryGetAll(id);
        return this.http.get(url);
    }

    communityAdvisoryEditPost(data): any {
        const url = this.configApi.communityAdvisoryEditPost();
        return this.http.put(url, data);
    }
    advisoryOntickeer(): any {
        const url = this.configApi.advisoryOntickeer();
        return this.http.get(url);
    }
    // search revamp
    SearchForContrubutors(counter, contributor): any {
        const url = this.configApi.SearchForContrubutors(counter, contributor);
        return this.http.get(url);
    }
    saearchForCollection(counter, collection): any {
        const url = this.configApi.saearchForCollection(counter, collection);
        return this.http.get(url);
    }
    searchForIndicator(counter, indicator): any {
        const url = this.configApi. searchForIndicator(counter, indicator);
        return this.http.get(url);
    }
}
