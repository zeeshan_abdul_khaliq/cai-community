import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConfigApiService} from './configApi.service';
import * as dayjs from 'dayjs';
import {LocaleConfig} from 'ngx-daterangepicker-material';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';
import {SelectedDate, SelectedDateInterfaceForSetting} from './community-interfaces';

@Injectable()
export class CalendarSettingService implements OnInit {
    role: string;
    locale: LocaleConfig = {
        format: 'YYYY-MM-DDTHH:mm:ss.SSSSZ',
        displayFormat: 'YYYY-MM-DD',
    };
    ranges: any = {
        Today: [dayjs(), dayjs()],
        Yesterday: [dayjs().subtract(1, 'days'), dayjs().subtract(1, 'days')],
        'Last 3 Days': [dayjs().subtract(3, 'days'), dayjs()],
        'Last Week': [dayjs().subtract(6, 'days'), dayjs()],
        'Last Month': [dayjs().subtract(30, 'days'), dayjs()],
        'Last 3 Months': [dayjs().subtract(3, 'month'), dayjs()],
        All: ['2019-01-01T00:00:00', dayjs()],
    };
    vpRanges: any = {
        Today: [dayjs(), dayjs()],
        'Last 3 Days': [dayjs().subtract(3, 'days'), dayjs()],
        'Last Week': [dayjs().subtract(6, 'days'), dayjs()],
        'Last Month': [dayjs().subtract(30, 'days'), dayjs()],
        All: ['2000-01-01T00:00:00', dayjs()],
    };
    aptRanges: any = {
        'Last Week': [dayjs().subtract(6, 'days'), dayjs()],
        'Last Month': [dayjs().subtract(30, 'days'), dayjs()],
        'Last 3 Months': [dayjs().subtract(3, 'month'), dayjs()],
        'Last Year': [dayjs().subtract(12, 'month'), dayjs()],
        All: ['1971-01-01T00:00:00', dayjs()],
    };
    selected: SelectedDateInterfaceForSetting;
    reportselected: SelectedDateInterfaceForSetting;
    vpDasboardCalendarSelected: SelectedDateInterfaceForSetting;
    exploreInVulnerabilityDateSelected: SelectedDateInterfaceForSetting;

    constructor(private http: HttpClient, private configApi: ConfigApiService, private datePi: DatePipe,
                private router: Router) {
        this.selected = {
            startDate: '2019-01-01T00:00:00',
            endDate: dayjs()
        };
        this.exploreInVulnerabilityDateSelected = {
            startDate: '2000-01-01T00:00:00',
            endDate: dayjs()
        };
        this.vpDasboardCalendarSelected = {
            startDate: '2000-01-01T00:00:00',
            endDate: dayjs()
        };
        this.reportselected = {
            startDate: dayjs().subtract(3, 'days'),
            endDate: dayjs()
        };
    }

    ngOnInit() {
    }

    transformDate(date) {
        return this.datePi.transform(date, 'yyyy-MM-ddThh:mm:ss');
    }
    transformRDate(date) {
        return this.datePi.transform(date, 'dd-MMM-yyyy');
    }
    transformAptDate(date) {
        return this.datePi.transform(date, 'yyyy-MM-dd');
    }

    transformNumbers(count) {
        if (count >= 1000 && count <= 99999) {
            count = count.toString().replace(/\B(?=(\d{3})+(?!\d))/g,
                ',');
        } else if (count >= 100000 && count <= 999999999) {
            count = (count / 1000000);
            count = Math.round(count * 100) / 100 + ' M';
        } else if (count >= 1000000000 && count <= 999999999999) {
            count = (count / 1000000000);
            count = Math.round(count * 10) / 10 + ' B';
        }
        return count;
    }
    getriskFactors() {
        const risk = [
            {
                riskFactor: 'Sinkholed',
                details: 'Domain has been sinkholed',
                risk: 'Critical'
            },
            {
                riskFactor: 'Associated with APT',
                details: 'IP, Domain, URL or Hash is linked with one or more APTs and has been used in one or more APT Attacks.',
                risk: 'Critical'
            },

            {
                riskFactor: 'Direct-to-IP URL',
                details: 'URL contains IP instead of domain',
                risk: 'High'
            },
            {
                riskFactor: 'Non-standard port',
                details: 'URL contains a non-standard port',
                risk: 'High'
            },
            {
                riskFactor: 'SSL Certificate Expired',
                details: 'SSL certificate of IP or Domain is expired',
                risk: 'High'
            },
            {
                riskFactor: 'Self-signed SSL certificate',
                details: 'SSL certificate of IP or Domain is self signed',
                risk: 'High'
            },
            {
                riskFactor: 'Suspicious SSL certificate',
                details: 'SSL certificate issuer is also the issuer of IP adressed or Domains with risk Medium or above',
                risk: 'High'
            },
            {
                riskFactor: 'Found in Multiple Threat Feeds',
                details: 'IP, Domain or URL is found in more than one threat feeds from multiple sources',
                risk: 'High'
            },
            {
                riskFactor: 'Found in CDP Feeds',
                details: 'IP is found in Cydea Deception Platform threat feed',
                risk: 'High'
            },
            {
                riskFactor: 'Detected as Malicious by AVs',
                details: 'IP, Domain, URL or Hash has been detected as malicious by one or more Antiviruses.',
                risk: 'High'
            },
            {
                riskFactor: 'Detected as Suspicious by AVs',
                details: 'IP, Domain, URL or Hash has been detected as suspicious by one or more Antiviruses.',
                risk: 'Medium'
            },
            {
                riskFactor: 'Found in Threat Feed',
                details: 'IP, Domain or URL is found in a single threat feed from one of the sources',
                risk: 'Medium'
            },
            {
                riskFactor: 'Doesn\'t Resolve to an IP',
                details: 'Domain doesn’t resolve to any IP address',
                risk: 'Medium'
            },
            {
                riskFactor: 'Registration recently updated',
                details: 'Registration of IP or Domain has been updated within the previous month',
                risk: 'Medium'
            },
            {
                riskFactor: 'Recently Registered',
                details: 'IP or Domain has been registered within the previous month',
                risk: 'Medium'
            },
            {
                riskFactor: 'Hosted and registered in different countries',
                details: 'Domain is registered in one country and hosted in another',
                risk: 'Medium'
            },
            {
                riskFactor: 'Returns PTR Record',
                details: 'IP address is routable and returns PTR records',
                risk: 'Very Low'
            },
            {
                riskFactor: 'Linked IPs return PTR records',
                details: 'IP address(es) associated with a Domain is routable and returns PTR records',
                risk: 'Very Low'
            },
            {
                riskFactor: 'Linked to top 100K domain',
                details: 'Domain lies within Top 100K domains of the Cisco Umbrella\'s Top 1 Million Domains list ',
                risk: 'Very Low'
            },
            {
                riskFactor: 'Linked to top 10K domain',
                details: 'Domain lies within Top 10K domains of the Cisco Umbrella\'s Top 1 Million Domains list ',
                risk: 'Very Low'
            },
            {
                riskFactor: 'Linked to top 1K domain',
                details: 'Domain lies within Top 1K domains of the Cisco Umbrella\'s Top 1 Million Domains list ',
                risk: 'Very Low'
            },
            {
                riskFactor: 'Linked to top 100 domain',
                details: 'Domain lies within Top 100 domains of the Cisco Umbrella\'s Top 1 Million Domains list ',
                risk: 'Very Low'
            },
            {
                riskFactor: 'All linked indicators return PTR records',
                details: 'All IP addresses linked with a single Domain are routable and return PTR records',
                risk: 'Very Low'
            },
            {
                riskFactor: 'SPF Record Present',
                details: 'A TXT record is found in DNS',
                risk: 'Very Low'
            },



        ];
        return risk;

    }
    routeGaurd() {
        this.role = sessionStorage.getItem('role');
        if (this.role) {

            if (this.role === 'ROLE_SUPER_ADMIN' || this.role === 'ROLE_PROD_MANAGER' ||
                this.role === 'ROLE_PARTNERS') {
                return false;
            } else if (this.role === 'ROLE_SOC_ADMIN' || this.role === 'ROLE_L1_ANALYST' ||
                this.role === 'ROLE_L2_ANALYST' || this.role === 'ROLE_CLIENT_ADMIN' ||
                this.role === 'ROLE_CLIENT_ANALYST') {
                return true;
            }
        } else {
            localStorage.clear();
            sessionStorage.clear();
            this.router.navigate(['/auth']);
        }
    }
    checkToken() {
        this.role = sessionStorage.getItem('role');
        if (this.role) {
            return true;
        } else {
            localStorage.clear();
            sessionStorage.clear();
            this.router.navigate(['/auth']);
        }
    }
    treemapSetting() {
        return [
            {
                color: [
                    '#499b59',
                    '#289b59',
                    '#17753f',
                    '#0d552c',
                ],
                colorMappingBy: 'value',
            }
        ];
    }
}
