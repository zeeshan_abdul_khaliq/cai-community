import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {CommunityModule} from './community/community.module';
const routes: Routes = [];
@NgModule({
  imports: [RouterModule.forRoot(routes, {  }), CommunityModule],
  exports: [RouterModule],
})
export class AppRoutingModule { }
