import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityAdvisoryComponent } from './community-advisory.component';

describe('CommunityAdvisoryComponent', () => {
  let component: CommunityAdvisoryComponent;
  let fixture: ComponentFixture<CommunityAdvisoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommunityAdvisoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityAdvisoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
