import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityService} from '../../core-services/community.service';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';

@Component({
    selector: 'app-community-advisory',
    templateUrl: './community-advisory.component.html',
    styleUrls: ['./community-advisory.component.scss'],
    providers: [DialogService, DynamicDialogRef]
})
export class CommunityAdvisoryComponent implements OnInit, OnDestroy {

    indicatorData: any;
    id: any;
    details: any;
    ref: DynamicDialogRef;
    reportType: any;
    downloadType: any;
    show = false;
    tags: any = [];
    userRole: any;
    routing: boolean;

    constructor(private communityService: CommunityService, private toastr: ToastrService, private router: Router,
                private spinner: NgxSpinnerService, private route: ActivatedRoute, public dialogService: DialogService,
                private config: DynamicDialogConfig) {
        if (!this.id) {
            this.id = config.data.value;
            this.routing = config.data.routing;
        }
        this.userRole = sessionStorage.getItem('role');
    }

    ngOnInit(): void {
        this.getData();
    }

    expireIT(id) {
        this.spinner.show('comadvidialog-spinner');
        this.communityService.communityAdvisoryExpire(id).subscribe(
            res => {
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.success('Advisory has been expired successfully');
                this.getData();
            },
            err => {
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.error('Unable to Expire this Advisory Kindly try again');
            });
    }

    deleteIT(id) {
        this.spinner.show('comadvidialog-spinner');
        this.communityService.communityAdvisotyDelete(id).subscribe(
            res => {
                this.communityService.savehistory();
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.success('Advisory Deleted successfully');
            },
            err => {
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.error('Unable to Delete Advisory Kindly try again');
            }
        );
    }

    edit(id) {
        this.router.navigate(['/community/updateAdvisory'], {queryParams: {id}});
    }

    getData() {
        this.spinner.show('comadvidialog-spinner');
        this.tags = [];
        this.communityService.updateAdvisoryGetAll(this.id).subscribe(
            res => {
                this.spinner.hide('comadvidialog-spinner');
                this.indicatorData = res;
                this.show = true;
                this.details = this.indicatorData.indicatorDetails;
                if (this.indicatorData && this.indicatorData.length !== 0) {
                    for (let i = 0; i < this.indicatorData.advisoryTags.length; i++) {
                        const data = {
                            name: this.indicatorData.advisoryTags[i].tag.name
                        };
                        this.tags.push(data);
                    }
                }
            }, err => {
                this.show = false;
                this.spinner.hide('comadvidialog-spinner');
                const error = err;
                if (error === 400) {
                    this.toastr.error(error.error.message);
                } else {
                    this.toastr.error('Oops Problem Occurred. Internal Server Error');
                }
            }
        );
    }

    openDialog(value) {
        this.ref = this.dialogService.open(CommunityTagsComponent, {
            header: 'Tag Appearance',
            width: '80%',
            contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value}
        });
    }

    ngOnDestroy(): void {
        this.router.navigate(['/community/advisory']);

    }
}
