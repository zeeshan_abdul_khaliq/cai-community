import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CommunityRoutingModule} from './community-routing.module';
import {CommunityComponent} from './community.component';
import {TableModule} from 'primeng/table';
import {CardModule} from 'primeng/card';
import {NgxSpinnerModule} from 'ngx-spinner';
import {TooltipModule} from 'primeng/tooltip';
import {TabViewModule} from 'primeng/tabview';
import {NgxEchartsModule} from 'ngx-echarts';
import * as echarts from 'echarts';
import {CalendarModule} from 'primeng/calendar';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MultiSelectModule} from 'primeng/multiselect';
import {AvatarModule} from 'primeng/avatar';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TagModule} from 'primeng/tag';
import {AdvisoryComponent} from './Advisory/advisory.component';
import {CreateAdvisoryComponent} from './Advisory/createAdvisory/createAdvisory.component';
import {EditorModule} from 'primeng/editor';
import {ChipsModule} from 'primeng/chips';
import {UpdateAdvisoryComponent} from './Advisory/updateAdvisory/updateAdvisory.component';
import {CollectionsComponent} from './Collections/collections.component';
import {CreateCollectionsComponent} from './Collections/createCollections/createCollections.component';
import {UpdateCollectionsComponent} from './Collections/updateCollections/updateCollections.component';
import {TopStatisticsComponent} from './dashboard/top-statistics/top-statistics.component';
import {TopContributorsComponent} from './dashboard/top-contributors/top-contributors.component';
import {TopTagsComponent} from './dashboard/top-tags/top-tags.component';
import {RecentCollectionsComponent} from './dashboard/recent-collections/recent-collections.component';
import {ExploreComponent} from './explore/explore.component';
import {ContributorsComponent} from './contributors/contributors.component';
import {IocTableComponent} from './contributors/ioc-table/ioc-table.component';
import { CommunitySearchComponent } from './community-search/community-search.component';
import {BadgeModule} from 'primeng/badge';
import {ScrollAdvisoryComponent} from './scrollAdvisory/scroll-advisory.component';
import {TagBaseAdvisoryListComponent} from './Advisory/tagBaseAdvisoryList/tagBaseAdvisoryList.component';
import {ComponentsModule} from '../framework/components/components.module';
import {CoreServicesModule} from '../core-services/core-services.module';
import {CommunityAdvisoryComponent} from './community-advisory/community-advisory.component';
import {CommunityCollectionsComponent} from './community-collections/community-collections.component';
import {CommunityContributorsComponent} from './community-contributors/community-contributors.component';
import {CommunityTagsComponent} from './community-tags/community-tags.component';
import {CommunityIocsComponent} from './community-iocs/community-iocs.component';
import {AuthGuard} from '../core-services/auth.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from '../core-services/token.interceptor.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';

@NgModule({
    declarations: [
        CommunityComponent, DashboardComponent, AdvisoryComponent, CreateAdvisoryComponent, UpdateAdvisoryComponent,
        CollectionsComponent, CreateCollectionsComponent, UpdateCollectionsComponent, TopStatisticsComponent,
        TopContributorsComponent, TopTagsComponent, RecentCollectionsComponent, ExploreComponent, ContributorsComponent,
        IocTableComponent, CommunitySearchComponent, ScrollAdvisoryComponent, TagBaseAdvisoryListComponent,
        CommunityAdvisoryComponent, CommunityCollectionsComponent, CommunityContributorsComponent, CommunityTagsComponent,
        CommunityIocsComponent
    ],
    exports: [
        CommunitySearchComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CommunityRoutingModule,
        ComponentsModule,
        CoreServicesModule,
        NgxSpinnerModule,
        TooltipModule,
        TabViewModule,
        TableModule,
        CardModule,
        CalendarModule,
        AvatarModule,
        EditorModule,
        ToastrModule,
        ChipsModule,
        DynamicDialogModule,
        ConfirmDialogModule,
        TagModule,
        MultiSelectModule,
        NgxDaterangepickerMd.forRoot(),
        NgxEchartsModule.forRoot({
            echarts,
        }),
        BadgeModule
    ], providers: [AuthGuard, {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true,
    },
        ToastrService],
})
export class CommunityModule {
}
