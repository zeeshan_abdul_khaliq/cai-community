import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {CommunityService} from '../../../core-services/community.service';
import {CommunityTagsComponent} from '../../community-tags/community-tags.component';
import {CommunityAdvisoryComponent} from '../../community-advisory/community-advisory.component';

@Component({
  selector: 'app-tagBaseAdvisoryList',
  templateUrl: './tagBaseAdvisoryList.component.html',
  styleUrls: ['../advisory.component.scss'],
  providers: [DialogService, DynamicDialogRef]

})
export class TagBaseAdvisoryListComponent implements OnInit, AfterViewInit {
  deleteLoader = false;
  tagDetail = false;
  tagspin = false;
  tagName: any;
  eventType: any;
  indicatorData: any;
  error: any;
  userDetail: any = false;
  userName: any;
  collections: any ;
  tag: any;
  constructor(private spinner: NgxSpinnerService, private communityservice: CommunityService,
              private tagRef: DynamicDialogRef, private dialogService: DialogService,
              private ref: DynamicDialogRef,
              private toastr: ToastrService,  private route: Router, private router: ActivatedRoute) {
    const event = this.router.snapshot.queryParamMap.get('typ');
    this.eventType = event;

  }
  ngOnInit() {

  }

  ngAfterViewInit() {
    this.router.queryParams.subscribe(params => {
      if (params.tag) {
        this.tag = params.tag;
        this.spinner.show('advisory-spinner');
        this.communityservice.communityAdvisoryByTag(this.tag).subscribe(
            res => {
              this.spinner.hide('advisory-spinner');
              this.indicatorData = res;
            }, err => {
              this.spinner.hide('advisory-spinner');

              this.error = err;
              this.toastr.error('Oops some problem occurred. Please wait');
            }
        );
      }
    });


  }
updateAdvisory(id) {
    this.route.navigate(['community/updateAdvisory'], {queryParams: {id}});
  }

  showLinks() {
    const role = sessionStorage.getItem('role');
    return true;
  }
  getRole() {
    const role = sessionStorage.getItem('role');
    if (role === 'ROLE_SUPER_ADMIN') {
      return true;

    } else if (role === 'ROLE_PROD_MANAGER') {
      return true;

    }
    else if (role === 'ROLE_PARTNERS') {
      return true;

    }
    else if (role === 'ROLE_SOC_ADMIN') {
      return true;

    }
    else if (role === 'ROLE_L1_ANALYST') {
      return false;

    } else if (role === 'ROLE_L2_ANALYST') {
      return true;

    } else if (role === 'ROLE_CLIENT_ADMIN') {
      return true;

    } else if (role === 'ROLE_CLIENT_ANALYST') {
      return true;

    }
  }
  tagDetails(tag) {
    this.tagRef = this.dialogService.open(CommunityTagsComponent, {
      header: 'Tag Appearance',
      width: '80%',
      contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: tag}
    });
  }
  userDetails(id) {
    this.ref = this.dialogService.open(CommunityAdvisoryComponent, {
      header: 'Advisory Details',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {value: id}
    });
  }
}


