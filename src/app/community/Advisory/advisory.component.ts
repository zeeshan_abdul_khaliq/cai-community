import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {Table} from 'primeng/table';
import {ConfirmationService} from 'primeng/api';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {TabView} from 'primeng/tabview';
import {CommunityService} from '../../core-services/community.service';
import {CommunityAdvisoryComponent} from '../community-advisory/community-advisory.component';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';
@Component({
  selector: 'app-advisory',
  templateUrl: './advisory.component.html',
  styleUrls: ['./advisory.component.scss'],
  providers: [ConfirmationService, DialogService, DynamicDialogRef]
})

export class AdvisoryComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table | undefined;

  indicatorData: any;
  error: any;
  tagDetail: any = false;
  tagspin: any = false;
  deleteLoader: any = false;
  response: any;
  active: any;
  expire: any;
  userRole: any;
  ref: DynamicDialogRef;
  tagRef: DynamicDialogRef;
  tag: any;
  index = 0;
  @ViewChild(TabView) tabView: TabView;
  constructor(private communityservice: CommunityService, private toastr: ToastrService, private router: Router,
              private confirmationService: ConfirmationService, public dialogService: DialogService,
              private spinner: NgxSpinnerService, private route: ActivatedRoute) {
    this.userRole = sessionStorage.getItem('role');
    this.route.queryParams.subscribe(params => {
      if (params.tag) {
        this.tag = params.tag;
      }
    });
    console.log('tag', this.tag);
    this.getAdvisory();

  }
  ngOnInit(): void {
  }
  getAdvisory() {
    this.active = [];
    this.expire = [];
    const role = sessionStorage.getItem('role');
    if (role === 'ROLE_SOC_ADMIN' || role === 'ROLE_L1_ANALYST' || role === 'ROLE_L2_ANALYST' || role === 'ROLE_CLIENT_ADMIN' || role === 'ROLE_CLIENT_ANALYST') {
      this.spinner.show('advisory-spinner');
      this.communityservice.communityAdvisotyFindALL(this.tag).subscribe(
          res => {
            this.spinner.hide('advisory-spinner');
            this.indicatorData = res;
            if (this.indicatorData) {
              this.active = [];
              this.expire = [];
              for (const obj of this.indicatorData) {
                if (obj.status === true || obj.status === 'true') {
                  this.active.push(obj);
                } else if (obj.status === false || obj.status === 'false') {
                  this.expire.push(obj);
                }
              }
            }
          }, err => {
            this.spinner.hide('advisory-spinner');

            this.error = err;
            this.toastr.error('Oops some problem occurred. Please wait');
          }
      );
    }
  }

  updateAdvisory(id) {
    this.router.navigate(['community/updateAdvisory'], {queryParams: {id}});
  }

  deleteAdvisory(id) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this Advisory?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.spinner.show('advisory-spinner');

        this.communityservice.communityAdvisotyDelete(id).subscribe(
            res => {
              this.communityservice.savehistory();
              this.spinner.hide('advisory-spinner');
              this.toastr.success('Advisory Deleted successfully');
              this.getAdvisory();
            },
            err => {
              this.spinner.hide('advisory-spinner');
              this.error = err;
              this.toastr.error('Oops some problem occurred. Please wait');
            }
        );
      },
      reject: () => {
      }
    });

  }

  openDialog(value) {
    this.ref = this.dialogService.open(CommunityAdvisoryComponent, {
      header: 'Advisory Details',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {value}
    });
    this.ref.onClose.subscribe((response: any) => {
      if (response) {
        this.getAdvisory();
      }
    });
  }

  indicatorClick(indicator, tagName) {
    this.router.navigate(['community/explore/type'], {queryParams: {id: indicator, tag: tagName}});
  }

  showLinks() {
    const role = sessionStorage.getItem('role');
    if (role === 'ROLE_SOC_ADMIN' || role === 'ROLE_L1_ANALYST' || role === 'ROLE_L2_ANALYST'
        || role === 'ROLE_CLIENT_ADMIN' || role === 'ROLE_CLIENT_ANALYST') {
      return true;
    } else {
      this.router.navigate(['community/dashboard']);
    }
  }

  tagDetails(tag) {
    this.tagDetail = true;
    this.tagspin = true;
    sessionStorage.setItem('tag', tag);

  }

  openModalDialog() {
    this.tagDetail = true;
  }

  userDetails(id) {
    sessionStorage.setItem('titleDetails', id);

  }

  openTagDialog(value) {
    this.ref = this.dialogService.open(CommunityTagsComponent, {
      header: 'Tag Appearance',
      width: '80%',
      contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: value}
    });
    this.ref.onClose.subscribe((response: any) => {
      if (response) {
        this.getAdvisory();
      }
    });
  }

  createAdvisory() {
    this.router.navigate(['/community/createAdvisory']);
  }

  ngOnDestroy(): void {
    if (this.ref) {
      this.ref.close();
    }
    if (this.tagRef) {
      this.tagRef.close();
    }
  }
}
