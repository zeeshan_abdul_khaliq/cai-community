import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';
import {Table} from 'primeng/table';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {DatePipe} from '@angular/common';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityService} from '../../../core-services/community.service';
import {CommunityAdvisoryComponent} from '../../community-advisory/community-advisory.component';

@Component({
    selector: 'app-createadvisory',
    templateUrl: './createAdvisory.component.html',
    styleUrls: ['./createAdvisory.component.scss'],
    providers: [MessageService, DialogService, DynamicDialogRef]
})

export class CreateAdvisoryComponent implements OnInit {
    fortag: any;
    tagss: any;
    descriptionAdvisory: any;
    summaryAdvisory: any;
    success: any;
    error: any;
    date: any;
    minDate: any;
    tagsrequired: any = false;
    ref: DynamicDialogRef;
    constructor(private communityService: CommunityService, private router: Router, private toastr: ToastrService,
                private spinner: NgxSpinnerService, private datePipe: DatePipe, public dialogService: DialogService, ) {
        this.tagss = [];
        this.date = new Date();
        this.minDate = new Date();
    }

    ngOnInit(): void {
    }

    add(event: any): void {
        const input = event.input;
        let value = event.value;

        // Add our fruit
        if ((value || '').trim()) {
            value = value.trim();
            if (this.tagss.find((test) => test.name.toLowerCase() === value.toLowerCase()) === undefined) {
                this.tagss.push({name: value.trim()});
            }

        }


        // Reset the input value
        if (input) {
            input.value = '';
        }
    }


    remove(event: any): void {
        console.log(event.value);
        const index = this.tagss.findIndex(x => x.name === event.value);

        if (index >= 0) {
            this.tagss.splice(index, 1);
        }
        console.log(this.tagss);
    }

    onSubmit(event) {
        this.fortag = [];
        if (this.tagss && this.tagss.length === 0) {
            this.tagsrequired = true;
        } else if (this.tagss && this.tagss.length !== 0) {
            this.tagsrequired = false;
            for (let i = 0; i < this.tagss.length; i++) {
                this.fortag.push({name: this.tagss[i].name});
            }
        }
        const username = sessionStorage.getItem('username');
        let datee;
        this.spinner.show('createAdvisory-spinner');
        datee = this.datePipe.transform(event.expirydate, 'yyyy-MM-dd');
        datee = datee + 'T23:59:59';
        const contentSummary = this.stripHtml(this.summaryAdvisory);
        const contentDescription = this.stripHtml(this.descriptionAdvisory);
        if (contentDescription) {
        } else {
            this.summaryAdvisory = null;
        }
        if (contentSummary) {
        } else {
            this.descriptionAdvisory = null;
        }
        const data = {
            details: this.descriptionAdvisory,
            expireDate: datee,
            summary: this.summaryAdvisory,
            tags: this.fortag,
            title: event.title,
        };
        this.communityService.communityAdvisoryPost(data).subscribe(res => {
                this.spinner.hide('createAdvisory-spinner');
                this.communityService.savehistory();
                this.communityService.exploreUpdate();
                this.success = res;
                this.tagss = [];
                this.toastr.success('Advisory Created Successfully');
                this.openTagDialog(this.success.id);

            },
            err => {
                this.spinner.hide('createAdvisory-spinner');
                this.error = err;
                if (this.error.status === 417) {
                    this.toastr.error('Failed to Insert the Advisory => Reason: ' + this.error.error.msg);
                } else if (this.error.status === 409) {
                    this.toastr.error('Advisory Already Exist' + this.error.error.msg);
                } else {
                    this.toastr.error('Oops some problem occurred. Please wait');

                }
            });
    }

    openTagDialog(value) {
        this.ref = this.dialogService.open(CommunityAdvisoryComponent, {
            header: 'Advisory Details',
            width: '80%',
            contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {value, routing: true}
        });
    }

    stripHtml(html) {
        let temporalDivElement = document.createElement('div');
        temporalDivElement.innerHTML = html;
        return temporalDivElement.textContent || temporalDivElement.innerText || '';
    }

    cancel() {
        this.router.navigate(['/community/advisory']);
    }

    ngOnDestroy(): void {
        if (this.ref) {
            this.ref.close();
        }
    }
}

