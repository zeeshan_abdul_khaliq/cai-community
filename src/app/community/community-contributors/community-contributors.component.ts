import {Component, OnDestroy, OnInit} from '@angular/core';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {CommunityService} from '../../core-services/community.service';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';
import {CommunityCollectionsComponent} from '../community-collections/community-collections.component';

@Component({
  selector: 'app-community-contributors',
  templateUrl: './community-contributors.component.html',
  styleUrls: ['./community-contributors.component.scss'],
  providers: [DialogService, DynamicDialogRef]
})
export class CommunityContributorsComponent implements OnInit, OnDestroy {

  show = false;
  indicatorData: any;
  collectionResponses: any;
  value: any;
  ref: DynamicDialogRef;
  colRef: DynamicDialogRef;
  error: any;
  counter = 0;
  constructor(private communityService: CommunityService, private toastr: ToastrService, private router: Router,
              private spinner: NgxSpinnerService, private route: ActivatedRoute, public dialogService: DialogService,
              private config: DynamicDialogConfig) {
    if (!this.value) {
      this.value = config.data.value;
    }
    this.counter = 10;
  }

  ngOnInit(): void {
    this.getContributors();
  }

  getContributors() {
    this.spinner.show('explore-spinner');
    this.communityService.SearchForContrubutors(this.counter, this.value).subscribe(
        res => {
          this.spinner.hide('explore-spinner');
          if (res) {
            this.indicatorData = res;
            this.indicatorData = this.indicatorData.collectionResponses;
          }
        }, err =>
        {
          this.spinner.hide('explore-spinner');
          this.error = err;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else {
            this.toastr.error('Oops Problem Occurred. Internal Server Error');
          }
        }
    );
  }

  openCollectionsDialog(value) {
    this.colRef = this.dialogService.open(CommunityCollectionsComponent, {
      header: 'Collections',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data: {type: value, modal: 'list'}
    });
  }


  openDialog(value) {
    this.ref = this.dialogService.open(CommunityTagsComponent, {
      header: 'Indicator Appearance',
      width: '80%',
      contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data: {type: value}
    });
  }

  ngOnDestroy(): void {
    if (this.ref) {
      this.ref.close();
    }
    if (this.colRef) {
      this.colRef.close();
    }
  }
}
