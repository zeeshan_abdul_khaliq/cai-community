import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityContributorsComponent } from './community-contributors.component';

describe('PatchHistoryComponent', () => {
  let component: CommunityContributorsComponent;
  let fixture: ComponentFixture<CommunityContributorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommunityContributorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityContributorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
