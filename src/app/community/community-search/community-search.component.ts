import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {CommunityService} from '../../core-services/community.service';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';
import {CommunityContributorsComponent} from '../community-contributors/community-contributors.component';
import {CommunityCollectionsComponent} from '../community-collections/community-collections.component';
@Component({
    selector: 'app-community-search',
    templateUrl: './community-search.component.html',
    styleUrls: ['./community-search.component.scss'],
    providers: [DialogService, DynamicDialogRef, DynamicDialogRef]
})
export class CommunitySearchComponent implements OnInit {

    @ViewChild('dt') dt: Table | undefined;
    ref: DynamicDialogRef;
    tagRef: DynamicDialogRef;
    dataSources: any[] = [];
    exploreData: any;
    type: any;
    typehtml: any;
    counter = 0;
    totalRecords: any = 0;
    country: any;
    error: any;
    tCounts: any;
    response: any;
    details: any;
    value: any;
    show: any = false;
    exportReporting: any = false;
    reportType: any;
    downloadType: any;
    tag: any;

    constructor(private toastr: ToastrService, private router: Router,
                public dialogService: DialogService, private communityService: CommunityService,
                private spinner: NgxSpinnerService, private route: ActivatedRoute) {
        this.counter = 0;
        this.route.queryParams.subscribe(params => {
            if (params.type && params.value) {
                this.type = params.type;
                this.counter = 10;
                this.type = params.type;
                this.typehtml = params.type;
                this.value = params.value;
                if (this.type === 'Collections') {
                    this.getData();
                } else if (this.type === 'Contributors') {
                    this.getContributors();
                } else if (this.type === 'Tag') {
                    this.getTagData();
                } else {
                    this.getExplore();
                }
            }
        });
    }

    ngOnInit(): void {
    }

    routeToComp(type) {
        if (type === 'Advisories') {
            this.router.navigate(['/community/apptagbaseAdvisory'], {queryParams: {tag: this.value}});
        } else if (type === 'Collections') {
            this.router.navigate(['/community/explore'], {queryParams: {type: 'Collections', tag: this.value}});
        }
    }

    downloadCSV(title) {
        this.exportReporting = true;
        this.reportType = 'application/csv';
        this.downloadType = title + '.csv';
        this.communityService.downloadCSV(this.value)
            .subscribe(
                res => {
                    const response: any = res;
                    const file = new Blob([response], {
                        type: this.reportType,
                    });
                    const fileURL = window.URL.createObjectURL(file);
                    const link = document.createElement('a');
                    link.href = fileURL;
                    link.download = this.downloadType;
                    document.body.appendChild(link);
                    link.click();
                    this.exportReporting = false;
                    this.toastr.success('Report download successful');
                },
                err => {
                    this.exportReporting = false;
                    const errors: any = err.status;
                    if (errors === 400) {
                        this.toastr.error('400 Bad Request: ' + errors.error.message);
                    } else  {
                        this.toastr.error('Oops some problem occurred. Please wait');
                    }
                });
    }

    getData() {
        this.details = [];
        this.spinner.show('explore-spinner');
        this.communityService.saearchForCollection(this.counter, this.value).subscribe(
            res => {
                this.spinner.hide('explore-spinner');
                const exploreData: any = res;
                this.exploreData = exploreData;
                if (this.exploreData) {

                    this.totalRecords = this.exploreData.totalElements;
                    this.details = exploreData.indicatorDetails;
                    this.dt.first = 0;

                } else {
                    this.exploreData = [];
                    this.details = [];
                }
            }, err => {
                this.show = false;
                this.spinner.hide('explore-spinner');
                const error = err;
                if (error === 400) {
                    this.toastr.error(error.error.message);
                } else {
                    this.toastr.error('Oops some problem occurred. Please wait');
                }
            }
        );
    }


    getExplore() {
        this.spinner.show('explore-spinner');
        this.communityService.searchForIndicator(this.counter, this.value).subscribe(
            res => {
                this.spinner.hide('explore-spinner');
                const exploreData: any = res;
                if (exploreData) {
                    this.totalRecords = exploreData.totalElements;
                    this.exploreData = exploreData.collectionResponses;
                    this.dt.first = 0;

                    if (exploreData.dataSources && exploreData.dataSources.length > 0) {
                        this.dataSources = exploreData.dataSources;
                    } else {
                        this.dataSources = [];
                    }
                }
            }, err => {
                this.spinner.hide('explore-spinner');
                this.error = err;
                if (this.error === 400) {
                    this.toastr.error(this.error.error.message);
                } else  {
                    this.toastr.error('Oops some problem occurred. Please wait');
                }
            }
        );
    }

    getTagData() {
        this.spinner.show('explore-spinner');
        this.communityService.communitygetTAGdetails(this.value).subscribe(
            res => {
                this.spinner.hide('explore-spinner');
                this.exploreData = res;
                if (this.exploreData) {
                    this.tCounts = this.exploreData.totalCount;
                    this.exploreData = this.exploreData.tagCounts;
                    this.dt.first = 0;

                }
            }, err => {
                this.spinner.hide('explore-spinner');
                this.error = err;
                if (this.error === 400) {
                    this.toastr.error(this.error.error.message);
                } else {
                    this.toastr.error('Oops some problem occurred. Please wait');
                }
            }
        );
    }

    getContributors() {
        this.spinner.show('explore-spinner');
        this.communityService.SearchForContrubutors(this.counter, this.value).subscribe(
            res => {
                this.spinner.hide('explore-spinner');
                if (res) {
                    this.exploreData = res;
                    this.totalRecords = this.exploreData.totalElements;

                    this.exploreData = this.exploreData.collectionResponses;
                    this.dt.first = 0;

                }
            }, err => {
                this.spinner.hide('explore-spinner');
                this.error = err;
                if (this.error === 400) {
                    this.toastr.error(this.error.error.message);
                } else {
                    this.toastr.error('Oops some problem occurred. Please wait');
                }
            }
        );
    }

    openTagDialog(value) {
        this.ref = this.dialogService.open(CommunityTagsComponent, {
            header: 'Tag Appearance',
            width: '80%',
            contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value}
        });
    }
    pagination(event) {
        this.counter = event.first / 10;
        console.log(this.counter);
        this.getExplore();
    }
    openDialog(value) {
        this.tagRef = this.dialogService.open(CommunityContributorsComponent, {
            header: 'Contributor',
            width: '90%',
            contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: this.type, value}
        });
    }

    openCollectionsDialog(value) {
        this.ref = this.dialogService.open(CommunityCollectionsComponent, {
            header: 'Collections',
            width: '90%',
            contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value, modal: 'list'}
        });
    }

    ngOnDestroy(): void {
        if (this.ref) {
            this.ref.close();
        }
        if (this.tagRef) {
            this.tagRef.close();
        }
    }

}
