import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {CommunityService} from '../../core-services/community.service';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';
import {CommunityIocsComponent} from '../community-iocs/community-iocs.component';
import {CommunityCollectionsComponent} from '../community-collections/community-collections.component';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.scss'],
  providers: [DialogService, DynamicDialogRef]
})
export class ExploreComponent implements OnInit, OnDestroy  {

  @ViewChild('dt') exploreTable: Table | undefined;

  exploreData: any;
  indicatorType: string;
  indicatorTypeForHtml: string;
  filter$: Observable<any>;
  counter = 0;
  totalRecords: number;
  country: string;
  sectorType: string;
  error: any;
  md5HashType: number;
  sha1HashType: number;
  sha256HashType: number;
  hashCountsResponse: any;
  spinnerReport = false;
  response: any;
  stixURL: string;
  tag: any;
  header = '';
  constructor(private toastr: ToastrService, private router: Router,
              private dynamicDialogRef: DynamicDialogRef, private tagRef: DynamicDialogRef,
              private iocRef: DynamicDialogRef,
              public dialogService: DialogService, private communityService: CommunityService,
              private spinner: NgxSpinnerService, private route: ActivatedRoute) {
    this.counter = 0;
    this.country = null;
    this.sectorType = null;
    this.filter$ = this.route.queryParamMap.pipe(
        map((params: ParamMap) => params.get('type')),
    );
    this.route.queryParams.subscribe(params => {
      if (params.tag) {
        this.tag = params.tag;
        this.dynamicDialogRef.close();
        this.iocRef.close();
        this.tagRef.close();
      }
    });
    this.filter$.subscribe(param => {
      this.counter = 0;
      this.indicatorType = param;
      this.indicatorTypeForHtml = param;
      this.header = this.indicatorTypeForHtml;
      if (this.indicatorType === 'Hash') {
        this.header = '';
        this.indicatorType = 'FileHash-MD5';
        this.gethashcounts();
        this.getExplore();
      } else if (this.indicatorType === 'Collections') {
        this.header = 'Collections';
        this.getCollections();
      } else if (this.indicatorType === 'Contributors') {
        this.header = 'Contributors';
        this.getContributors();
      } else {
        if (this.indicatorTypeForHtml === 'All') {
          this.header += ' IoCs';
        }
        this.getExplore();
      }
    });
  }
  ngOnInit(): void {
  }
  getExplore(): void {
    this.spinner.show('explore-spinner');
    this.communityService.communitygetIndicatorsByType(this.indicatorType, this.counter).subscribe(
        communitygetIndicatorsByTypeRes => {
          this.spinner.hide('explore-spinner');
          this.exploreData = communitygetIndicatorsByTypeRes;
          if (this.exploreData) {
            this.totalRecords = this.exploreData.totalElements;
            this.exploreData = [...this.exploreData.summaryData];
            if (this.counter === 0) {
              this.exploreTable.first = 0;
            }
          }else {
            this.totalRecords = 0;
            this.exploreData = [];
          }
        }, communitygetIndicatorsByTypeErr =>
        {
          this.spinner.hide('explore-spinner');
          this.error = communitygetIndicatorsByTypeErr;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else if (this.error === 500) {
            this.toastr.error('Oops Problem Occurred. Internal Server Error');
          }
        }
    );
  }
  getContributors(): void {
    this.spinner.show('explore-spinner');
    this.communityService.communitygetIndicatorsByContributors(this.counter).subscribe(
        communitygetIndicatorsByContributorsRes => {
          this.spinner.hide('explore-spinner');
          if (communitygetIndicatorsByContributorsRes) {
            this.exploreData = communitygetIndicatorsByContributorsRes;
            this.totalRecords = this.exploreData.length;
            if (this.counter === 0) {
              this.exploreTable.first = 0;
            }
          } else {
            this.totalRecords = 0;
            this.exploreData = [];
          }
        }, communitygetIndicatorsByContributorsErr =>
        {
          this.spinner.hide('explore-spinner');
          this.error = communitygetIndicatorsByContributorsErr;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else if (this.error === 500) {
            this.toastr.error('Oops Problem Occurred. Internal Server Error');
          }
        }
    );
  }

  openTagDialog(value) {
    this.tagRef = this.dialogService.open(CommunityTagsComponent, {
      header: 'Tag Appearance',
      width: '80%',
      contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: value}
    });
  }

  downloadMalware(ioc): void {
    const reporttype = 'application/octet-stream';
    this.communityService.communitygetIndicatorsMalware(ioc).subscribe(
        communitygetIndicatorsMalwareRes => {
              this.response = communitygetIndicatorsMalwareRes;
              const malwareFile = new Blob([this.response], {
                type: reporttype,
              });
              const fileURL = window.URL.createObjectURL(malwareFile);
              const malwareLink = document.createElement('a');
              malwareLink.href = fileURL;
              const downloadType = 'Malware ' + ioc;
              malwareLink.download = downloadType;
              malwareLink.click();
              this.toastr.success('Report download successful');
            },
        communitygetIndicatorsMalwareResErr => {
              this.spinner.hide('explore-spinner');
              this.error = communitygetIndicatorsMalwareResErr;
              this.toastr.error('Error: ', this.error.error.message);
            });
  }

  getCollections(): void {
    this.spinner.show('explore-spinner');
    this.communityService.communitygetIndicatorsByTypeCollections(this.counter, this.tag).subscribe(
        communitygetIndicatorsByTypeCollectionsRes => {
          this.spinner.hide('explore-spinner');
          this.exploreData = communitygetIndicatorsByTypeCollectionsRes;
          if (this.exploreData) {
            this.totalRecords = this.exploreData.totalElements;
            this.exploreData = [...this.exploreData.collectionResponses];
            if (this.counter === 0) {
              this.exploreTable.first = 0;
            }
          } else {
            this.totalRecords = 0;
            this.exploreData = [];
          }
        }, communitygetIndicatorsByTypeCollectionsErr => {
          this.spinner.hide('explore-spinner');
          this.error = communitygetIndicatorsByTypeCollectionsErr;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else if (this.error === 500) {
            this.toastr.error('Oops Problem Occurred. Internal Server Error');
          }
        }
    );
  }
  gethashcounts(): void {
    this.communityService.communityIndiactorExploreHashCount().subscribe(communityIndiactorExploreHashCountRes => {
      this.hashCountsResponse = communityIndiactorExploreHashCountRes;
      for (const hash of this.hashCountsResponse) {
        if (hash.type === 'FileHash-MD5') {
          this.md5HashType = hash.count;
        } else if (hash.type === 'FileHash-SHA256') {
          this.sha256HashType = hash.count;
        } else if (hash.type === 'FileHash-SHA1') {
          this.sha1HashType = hash.count;
        }
      }
    }, communityIndiactorExploreHashCountErr => {
      this.error = communityIndiactorExploreHashCountErr;
      this.toastr.error(this.error.error.message);
    });
  }
  pagination(event): void {
    this.counter  = event.first / 10;
    if (this.indicatorType === 'Collections') {
      this.getCollections();
    }
    else if (this.indicatorType === 'Contributors') {
      this.getCollections();
    } else {
      this.getExplore();
    }
  }
  changetab(event): void {
    this.counter = 0;
    if (event.index === 0) {
      this.indicatorType = 'FileHash-MD5';
    } else if (event.index === 1){
      this.indicatorType = 'FileHash-SHA1';
    } else if (event.index === 2) {
      this.indicatorType = 'FileHash-SHA256';
    }
    this.getExplore();
  }

  openDialog(value): void {
    this.iocRef = this.dialogService.open(CommunityIocsComponent, {
      header: 'Indicator Appearance',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: this.indicatorType, value}
    });
  }

  openContributorsDialog(email, cont) {
    this.router.navigate(['/community/contributor'], { queryParams: {email, contributor: cont}, skipLocationChange: true});
  }

  openCollectionsDialog(value) {
    this.dynamicDialogRef = this.dialogService.open(CommunityCollectionsComponent, {
      header: 'Collections',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: value, modal: 'list'}
    });
  }

  ngOnDestroy(): void {
    if (this.dynamicDialogRef) {
      this.dynamicDialogRef.close();
    }
    if (this.tagRef) {
      this.tagRef.close();
    }
    if (this.iocRef) {
      this.iocRef.close();
    }
  }
}
