import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityIocsComponent } from './community-iocs.component';

describe('CommunityIocsComponent', () => {
  let component: CommunityIocsComponent;
  let fixture: ComponentFixture<CommunityIocsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommunityIocsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityIocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
