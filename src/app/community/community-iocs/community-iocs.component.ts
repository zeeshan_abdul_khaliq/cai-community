import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityService} from '../../core-services/community.service';
import {CommunityCollectionsComponent} from '../community-collections/community-collections.component';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';

@Component({
    selector: 'app-community-iocs',
    templateUrl: './community-iocs.component.html',
    styleUrls: ['./community-iocs.component.scss'],
    providers: [DialogService, DynamicDialogRef]
})
export class CommunityIocsComponent implements OnInit, OnDestroy {
    show = false;
    indicatorData: any;
    collectionResponses: any;
    value: any;
    constructor(private communityService: CommunityService, private toastr: ToastrService, private router: Router,
                private spinner: NgxSpinnerService, private ref: DynamicDialogRef,
                private colRef: DynamicDialogRef, private iocRef: DynamicDialogRef,
                private route: ActivatedRoute, public dialogService: DialogService,
                private config: DynamicDialogConfig) {
        if (!this.value) {
            this.value = config.data.value;
        }
    }

    ngOnInit(): void {
        this.getData();
    }

    getData() {
        this.spinner.show('iocs-spinner');
        this.communityService.communityWorkSpaceIndicatorDetails(200, this.value).subscribe(
            res => {
                this.spinner.hide('iocs-spinner');
                if (res) {
                    this.show = true;
                    this.indicatorData = res;
                    if (this.indicatorData.otherContributors && this.indicatorData.otherContributors.length > 0) {
                        this.collectionResponses = this.indicatorData.otherContributors;
                    } else if (this.indicatorData.ownCollections && this.indicatorData.ownCollections.length > 0) {
                      this.collectionResponses = this.indicatorData.ownCollections;
                    }
                }
            }, err => {
                this.spinner.hide('iocs-spinner');
                const error = err;
                this.show = false;
                if (error === 400) {
                    this.toastr.error(error.error.message);
                } else {
                    this.toastr.error('Oops Problem Occurred. Internal Server Error');
                }
            }
        );
    }

    openCollectionsDialog(value) {
        this.iocRef.close();
        this.colRef = this.dialogService.open(CommunityCollectionsComponent, {
            header: 'Collections',
            width: '90%',
            contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value, modal: 'list'}
        });
    }

    openDialog(value) {
        this.iocRef.close();
        this.ref.close();
        this.colRef.close();
        this.ref = this.dialogService.open(CommunityTagsComponent, {
            header: 'Indicator Appearance',
            width: '80%',
            contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value}
        });
    }

    ngOnDestroy(): void {
        if (this.ref) {
            this.ref.close();
        }
        if (this.colRef) {
            this.colRef.close();
        }
    }
}
