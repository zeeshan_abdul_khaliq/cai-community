import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityService} from '../../core-services/community.service';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';

@Component({
    selector: 'app-community-collections',
    templateUrl: './community-collections.component.html',
    styleUrls: ['./community-collections.component.scss'],
    providers: [DialogService]
})
export class CommunityCollectionsComponent implements OnInit, OnDestroy {
    indicatorData: any;
    tagDetails: any;
    id: any;
    details: any;
    exportReporting: any = false;
    reportType: any;
    downloadType: any;
    show = false;
    modaltype: any;
    constructor(private communityService: CommunityService, private toastr: ToastrService,
                public ref: DynamicDialogRef,
                private router: Router,
                private spinner: NgxSpinnerService, private route: ActivatedRoute, public dialogService: DialogService,
                private config: DynamicDialogConfig) {
        if (!this.id) {
            this.id = config.data.type;
            this.modaltype = config.data.modal;
        }
    }

    ngOnInit(): void {
        this.getData();
    }

    downloadCSV(title) {
        this.exportReporting = true;
        this.reportType = 'application/csv';
        this.downloadType = title + '.csv';
        this.communityService.downloadCSV(this.id)
            .subscribe(
                res => {
                    const response: any = res;
                    const file = new Blob([response], {
                        type: this.reportType,
                    });
                    const fileURL = window.URL.createObjectURL(file);
                    const link = document.createElement('a');
                    link.href = fileURL;
                    link.download = this.downloadType;
                    document.body.appendChild(link);
                    link.click();
                    this.exportReporting = false;
                    this.toastr.success('Report download successful');
                },
                err => {
                    this.exportReporting = false;
                    const errors: any = err.status;
                    if (errors === 400) {
                        this.toastr.error('400 Bad Request: ' + errors.error.message);
                    } else {
                        this.toastr.error('500 Internal Error Server');
                    }
                });
    }

    getData() {
        this.spinner.show('collections-spinner');
        this.communityService.communityGetIndicatorDetails(this.id).subscribe(
            res => {
                this.spinner.hide('collections-spinner');
                this.indicatorData = res;
                this.show = true;
                this.details = this.indicatorData.indicatorDetails;
            }, err => {
                this.show = false;
                this.spinner.hide('collections-spinner');
                const error = err;
                if (error === 400) {
                    this.toastr.error(error.error.message);
                } else {
                    this.toastr.error('Oops Problem Occurred. Internal Server Error');
                }
            }
        );
    }

    openDialog(value) {
        this.ref = this.dialogService.open(CommunityTagsComponent, {
            header: 'Tag Appearance',
            width: '80%',
            contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data:  {type: value}
        });
    }
    ngOnDestroy(): void {
        if (this.ref) {
            this.ref.close();
            if (this.modaltype === 'create') {
                this.router.navigate(['/community/collections']);

            }
        }
    }
}
