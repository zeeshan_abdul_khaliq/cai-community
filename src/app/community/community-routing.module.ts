import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommunityComponent} from './community.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AdvisoryComponent} from './Advisory/advisory.component';
import {CreateAdvisoryComponent} from './Advisory/createAdvisory/createAdvisory.component';
import {UpdateAdvisoryComponent} from './Advisory/updateAdvisory/updateAdvisory.component';
import {CollectionsComponent} from './Collections/collections.component';
import {CreateCollectionsComponent} from './Collections/createCollections/createCollections.component';
import {UpdateCollectionsComponent} from './Collections/updateCollections/updateCollections.component';
import {ExploreComponent} from './explore/explore.component';
import {ContributorsComponent} from './contributors/contributors.component';
import {CommunitySearchComponent} from './community-search/community-search.component';
import {ScrollAdvisoryComponent} from './scrollAdvisory/scroll-advisory.component';
import {TagBaseAdvisoryListComponent} from './Advisory/tagBaseAdvisoryList/tagBaseAdvisoryList.component';

const routes: Routes = [
    {
        path: '',
        component: CommunityComponent,
        children: [
            { path: '',
              redirectTo: 'dashboard',
              pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'explore',
                component: ExploreComponent
            },
            {
                path: 'advisory',
                component: AdvisoryComponent
            },
            {
                path: 'apptagbaseAdvisory',
                component: TagBaseAdvisoryListComponent
            },
            {
                path: 'contributor',
                component: ContributorsComponent
            },
            {
                path: 'createAdvisory',
                component: CreateAdvisoryComponent
            },
            {
                path: 'updateAdvisory',
                component: UpdateAdvisoryComponent
            },
            {
                path: 'collections',
                component: CollectionsComponent
            },
            {
                path: 'createCollections',
                component: CreateCollectionsComponent
            },
            {
                path: 'updateCollections',
                component: UpdateCollectionsComponent
            },
            {
                path: 'search-ioc',
                component: CommunitySearchComponent
            },
            {
                path: 'view-advisory-scroll',
                component: ScrollAdvisoryComponent
            }
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CommunityRoutingModule {
}
