import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IocTableComponent } from './ioc-table.component';

describe('IocTableComponent', () => {
  let component: IocTableComponent;
  let fixture: ComponentFixture<IocTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IocTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IocTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
