import {AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {CommunityService} from '../../../core-services/community.service';
import {CommunityTagsComponent} from '../../community-tags/community-tags.component';
import {CommunityIocsComponent} from '../../community-iocs/community-iocs.component';
import {CommunityCollectionsComponent} from '../../community-collections/community-collections.component';

@Component({
    selector: 'app-ioc-table',
    templateUrl: './ioc-table.component.html',
    styleUrls: ['./ioc-table.component.scss'],
    providers: [DialogService, DynamicDialogRef]
})
export class IocTableComponent implements OnInit, OnChanges, OnDestroy {

    @ViewChild('dt') dt: Table | undefined;
    exploreData: any;
    type: any;
    counter = 0;
    totalRecords: any;
    country: any;
    error: any;
    response: any;
    @Input() data: any;
    ref: DynamicDialogRef;
    tagRef: DynamicDialogRef;
    collRef: DynamicDialogRef;
    constructor(public dialogService: DialogService, private communityService: CommunityService,
                private toastr: ToastrService, private router: Router,
                private spinner: NgxSpinnerService) {
        this.counter = 0;
        this.country = null;
    }

    ngOnInit(): void {
    }

    openTagDialog(value) {
        this.ref = this.dialogService.open(CommunityTagsComponent, {
            header: 'Tag Appearance',
            width: '80%',
            contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data:  {type: value}
        });
    }
    getExplore() {
        if (this.data) {
            this.type = this.data.type;
            const records = this.data.totalElements;
            this.totalRecords = records;
            console.log(this.totalRecords);
            this.exploreData = this.data.reponse;
        }
    }

    pagination(event) {
        this.counter = event.first / 10;
        this.getExplore();
    }

    openDialog(value) {
        this.tagRef = this.dialogService.open(CommunityIocsComponent, {
            header: 'Indicator Appearance',
            width: '90%',
            contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: this.type, value}
        });
    }

    openCollectionsDialog(value) {
        this.collRef = this.dialogService.open(CommunityCollectionsComponent, {
            header: 'Collection Details',
            width: '90%',
            contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value, modal: 'list'}
        });
    }
    ngOnDestroy(): void {
        if (this.ref) {
            this.ref.close();
        }
        if (this.tagRef) {
            this.tagRef.close();
        }
        if (this.collRef) {
            this.collRef.close();
        }
    }

    downloadMalware(ioc) {
        // const reporttype = 'application/pdf';
        const reporttype = 'application/octet-stream';
        this.communityService.communitygetIndicatorsMalware(ioc)
            .subscribe(
                res => {
                    this.response = res;
                    const file = new Blob([this.response], {
                        type: reporttype,
                    });
                    const fileURL = window.URL.createObjectURL(file);
                    const link = document.createElement('a');
                    link.href = fileURL;
                    const downloadType = 'Malware ' + ioc;
                    link.download = downloadType;
                    link.click();
                    this.toastr.error('Report download successful');
                    const content = 'Report has been Downloaded';
                },
                err => {
                    this.spinner.hide('explore-spinner');
                    this.error = err;
                    this.toastr.error('Oops some problem occurred. Please wait');
                });
    }

  ngOnChanges(changes: SimpleChanges): void {
    this.getExplore();
  }
}
