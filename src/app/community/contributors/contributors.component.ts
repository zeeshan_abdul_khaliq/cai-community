import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {TabView} from 'primeng/tabview';
import {IocTableComponent} from './ioc-table/ioc-table.component';
import {CommunityService} from '../../core-services/community.service';

@Component({
  selector: 'app-contributors',
  templateUrl: './contributors.component.html',
  styleUrls: ['./contributors.component.scss'],
})
export class ContributorsComponent implements OnInit {
  data: any;
  contributors: any;
  @ViewChild('dt') dt: Table | undefined;
  @ViewChild(IocTableComponent, {static: false}) childRef: IocTableComponent;
  response: any;
  type: any;
  typehtml: any;
  filter$: Observable<any>;
  counter = 10;
  totalRecords: any;
  country: any;
  sectorType: any;
  error: any;
  hashType_md5: any;
  hashType_sha1: any;
  hashType_sha256: any;
  hashcounts: any;
  email: any;
  contributor: any;
  malwareCounts: any;
  urlCounts: any;
  domainCounts: any;
  collectionCounts: any;
  ipCounts: any;
  emailCounts: any;
  show = false;
  selectedIndex = 0;
  @ViewChild(TabView) tabView: TabView;
  constructor(private toastr: ToastrService, private router: Router,
              private communityService: CommunityService,
              private spinner: NgxSpinnerService, private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.email = params.email;
      this.contributor = params.contributor;
    });
    this.getCounts();
  }
  ngOnInit(): void {
  }
  getCounts() {
    this.communityService.communityIndiactorContributorCount(this.contributor, true).subscribe(res => {
      const o: any = res;
      for (let i = 0; i < o.length; i++) {
        this.typehtml = o[i].type;
        if (o[i].type === 'Collections') {
          this.collectionCounts = o[i].count;
          this.counter = this.collectionCounts;
        } else if (o[i].type === 'Hash' || o[i].type === 'FileHash') {
          this.hashcounts = o[i].count;
        } else if (o[i].type === 'IPv4') {
          this.ipCounts = o[i].count;
        } else if (o[i].type === 'Domain') {
          this.domainCounts = o[i].count;
        } else if (o[i].type === 'URL') {
          this.urlCounts = o[i].count;
        } else if (o[i].type === 'Email') {
          this.emailCounts = o[i].count;
        } else if (o[i].type === 'Malware') {
          this.malwareCounts = o[i].count;
        }
      }
      if (this.collectionCounts > 0) {
        this.getCollections();
      } else {
        this.getExplore();
      }
    }, err => {
      this.error = err;

    });
  }
  getExplore() {
    this.spinner.show('explore-spinner');
    this.show = false;
    this.communityService.communityGetIndicatorBytypeContributor(this.typehtml, this.email, this.counter).subscribe(
        res => {
          this.spinner.hide('explore-spinner');
          const resp: any = res;
          this.response = { reponse: resp.summaryData, type: this.typehtml,  totalElements: resp.totalElements};
          this.response = {...this.response};
          this.show = true;
          if (this.childRef) {
            this.childRef.data = this.response;
          }
        }, err =>
        {
          this.spinner.hide('explore-spinner');
          this.error = err;
          this.show = true;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else {
            this.toastr.error('Oops some problem occurred. Please wait');
          }
        }
    );
  }
  getCollections() {
    this.spinner.show('explore-spinner');
    this.show = false;
    this.communityService.communityGetIndicatorBytypeContributor('Collections', this.email, this.counter).subscribe(
        res => {
          this.spinner.hide('explore-spinner');
          const resp: any = res;
          this.response = { reponse: resp.collectionResponses, type: 'Collections', totalElements: resp.totalElements};
          this.response = {...this.response};
          this.show = true;
          if (this.childRef) {
            this.childRef.data = this.response;
          }
        }, err =>
        {
          this.spinner.hide('explore-spinner');
          this.show = true;
          this.error = err;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else  {
            this.toastr.error('Oops some problem occurred. Please wait');
          }
        }
    );
  }
  pagination(event) {
    this.counter  = event.first / 10;
    console.log(this.counter);
    this.getExplore();
  }
  getSelectedHeader(event){
    this.selectedIndex = event.index;
    const str = this.tabView.tabs[this.selectedIndex].header;
    if (str.slice(0, 3) === 'Col') {
      this.counter = this.collectionCounts;

      this.getCollections();
    } else if (str.slice(0, 3) === 'IPv') {
      this.typehtml = 'IPv4';
      this.counter = this.ipCounts;

      this.getExplore();
    } else if (str.slice(0, 3) === 'Dom') {
      this.typehtml = 'Domain';
      this.counter = this.domainCounts;

      this.getExplore();
    }  else if (str.slice(0, 3) === 'URL') {
      this.typehtml = 'URL';
      this.counter = this.urlCounts;

      this.getExplore();
    } else if (str.slice(0, 3) === 'Ema') {
      this.typehtml = 'Email';
      this.counter = this.emailCounts;

      this.getExplore();
    } else if (str.slice(0, 3) === 'Mal') {
      this.typehtml = 'Malware';
      this.counter = this.malwareCounts;

      this.getExplore();
    } else if (str.slice(0, 3) === 'Fil') {
      this.typehtml = 'Hash';
      this.counter = this.hashcounts;

      this.getExplore();
    }
  }
}
