import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {DatePipe} from '@angular/common';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityService} from '../../../core-services/community.service';
import {CommunityCollectionsComponent} from '../../community-collections/community-collections.component';

@Component({
    selector: 'app-updateCollections',
    templateUrl: './updateCollections.component.html',
    styleUrls: ['./updateCollections.component.scss'],
    providers: [DialogService, DynamicDialogRef]
})

export class UpdateCollectionsComponent implements OnInit {
    collectionIoCForm: FormGroup;
    fortag: any;
    tags: any;
    descriptionAdvisory: any;
    success: any;
    error: any;
    date: any;
    minDate: any;
    id: any;
    fetchData: any;
    ref: DynamicDialogRef;
    tagsrequired: any = false;

    indicatorsName: any[] = [
        {label: 'IPv4', value: 'IPv4'},
        {label: 'URL', value: 'URL'},
        {label: 'FileHash-MD5', value: 'FileHash-MD5'},
        {label: 'FileHash-SHA1', value: 'FileHash-SHA1'},
        {label: 'FileHash-SHA256', value: 'FileHash-SHA256'},
        {label: 'Email', value: 'Email'},
        {label: 'Domain', value: 'Domain'}
    ];
    selectedIndicator: any;

    constructor(private communityService: CommunityService, private router: Router, private toastr: ToastrService,
                private route: ActivatedRoute, public dialogService: DialogService,
                private spinner: NgxSpinnerService, private datePipe: DatePipe, private formBuilder: FormBuilder) {
        this.tags = [];
        this.date = new Date();
        this.minDate = new Date();
        this.selectedIndicator = this.indicatorsName[0].value;
        this.collectionIoCForm = this.formBuilder.group({
            main: ['', [Validators.required, Validators.pattern('^[ A-Za-z0-9_.%,@#&$:_-]+$')]],
            description: ['', [Validators.required]],
            modifiedDate: ['', [Validators.required]],
            uploadDate: ['', [Validators.required]],
            tags: ['', [Validators.required]],
            indicators: this.formBuilder.array([this.indicator(this.selectedIndicator, '')])
        });
    }

    ngOnInit(): void {
        this.id = this.route.snapshot.queryParamMap.get('id');
        this.spinner.show('updateCollection-spinner');

        this.communityService.communityGetIndicatorDetails(this.id).subscribe(
            res => {
                this.fetchData = res;
                this.spinner.hide('updateCollection-spinner');
                if (this.fetchData) {
                    this.tags = this.fetchData.tagDetails;
                    this.uploadIndicatorsFromFile(this.fetchData.indicatorDetails, this.fetchData);

                }
            });
    }
    oncollectionIoCFormSubmit() {
        const collectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        for (let collectionIndex = 0; collectionIndex < collectionControl.controls.length; collectionIndex++){
            if (collectionControl.controls[collectionIndex].status === 'INVALID' || collectionControl.controls[collectionIndex].value.value === '') {
                collectionControl.removeAt(collectionIndex);
            }
        }
        if (this.collectionIoCForm.invalid) {
            this.toastr.error('Please complete the form');
            return;
        }
        const event = this.collectionIoCForm.value;
        this.fortag = [];
        if (this.tags && this.tags.length === 0) {
            this.tagsrequired = true;
        } else if (this.tags && this.tags.length !== 0) {
            this.tagsrequired = false;
            for (let tagIndex = 0; tagIndex < this.tags.length; tagIndex++) {
                this.fortag.push({name: this.tags[tagIndex].name});
            }
        }
        const username = sessionStorage.getItem('username');
        this.spinner.show('updateCollection-spinner');
        this.descriptionAdvisory = this.stripHtml(event.description);
        if (!this.descriptionAdvisory) {
            this.descriptionAdvisory = null;
        }
        const data = {
            description: this.descriptionAdvisory,
            indicators: event.indicators,
            tags: this.fortag,
            id: this.id,
            title: event.main,
            users: {
                email: username
            },
        };
        this.communityService.communityIndicatorPut(data).subscribe(res => {
                this.success = res;
                if (this.success.statusCode === 201 || this.success.statusCode === 200) {
                    this.spinner.hide('updateCollection-spinner');
                    this.success = res;
                    this.tags = [];
                    this.toastr.success('Collection Updated Successfully');
                    const id = this.success.id;
                    if (id) { this.openCollectionsDialog(id); }
                }
            },
            err => {
                this.spinner.hide('updateCollection-spinner');
                this.error = err;
                if (this.error.status === 417) {
                    this.toastr.error('Failed to Update the Collection => Reason: ' + this.error.error.msg);
                } else if (this.error.status === 400) {
                    this.toastr.error(this.error.error.msg);
                }else {
                    this.toastr.error('Oops some problem occurred. Please wait');
                }
            });


    }
    uploadIndicatorsFromFile(iocs, data) {
        this.fortag = [];
        if (data.tagDetails.length > 0) {
            for (const tag of data.tagDetails) {
                this.fortag.push(tag.name);
            }
        }
        this.collectionIoCForm.patchValue({
            main: data.title,
            description: data.description,
            modifiedDate: data.modifiedDate,
            uploadDate: data.uploadDate,
            tags: this.fortag,
        });
        const updateCollectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        let count = updateCollectionControl.length;
        if (count > 1) {
            updateCollectionControl.removeAt(count);
            updateCollectionControl[count] = [];
        } else {
            updateCollectionControl.removeAt(0);
            updateCollectionControl[0] = [];
            count = 0;
        }
        for (let iocIndex = 0; iocIndex < iocs.length; iocIndex++) {
            const edd = iocs[iocIndex];
            if ((edd.type !== null || edd.type !== undefined) &&  (edd.indicator !== null || edd.indicator !== undefined) ) {
                updateCollectionControl.push(this.setindicator(edd.type, edd.indicator));
                this.indicatorTypeOptionsForFile(count, edd.type);
                count++;
            }
        }
    }
    stripHtml(html) {
        const temporalDivElement = document.createElement('div');
        temporalDivElement.innerHTML = html;
        return temporalDivElement.textContent || temporalDivElement.innerText || '';
    }

    cancel() {
        this.router.navigate(['/community/collections']);
    }
    indicator(type, value): FormGroup {
        return this.formBuilder.group({
            type: [type, Validators.required],
            value: [value, [Validators.required, Validators.pattern(
                '^(?!0\.0\.0\.0/)(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/\d+)?$'
            )]],
        });
    }
    setindicator(type, value): FormGroup {
        return this.formBuilder.group({
            type: [type, Validators.required],
            value: [value, [Validators.required]],
        });
    }

    addIndicator(index) {
        const updateCollectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        let checkStatus = 0;
        for (const collectioValue of updateCollectionControl.controls){
            if (collectioValue.status === 'INVALID' || collectioValue.value.value === '') {
                checkStatus++;
            }
        }
        if (checkStatus < 2) {
            updateCollectionControl.push(this.indicator(updateCollectionControl.value[index].type, ''));
        }
    }

    getPlaceholder(iocIndex, event) {
        if (event && event.type && event.type.value) {
            switch (event.type.value) {
                case 'IPv4':
                    return 'Enter IP (192.16.0.12)';
                case 'FileHash-MD5':
                    return 'Enter Hash Md5 ';

                case 'FileHash-SHA1':
                    return 'Enter Hash SHA1 ';

                case 'FileHash-SHA256':
                    return 'Enter Hash SHA256 ';

                case 'URL':
                    return 'Enter URL e.g www.google.com';


                case 'Email':
                    return 'Enter Email (example@abc.com)';

                case 'Domain':
                    return 'Enter Domain e.g www.google.com';

            }

        }
    }

    indicatorTypeOptions(iocIndex, event) {

        const collectionControl = this.collectionIoCForm.get('indicators')['controls'];
        if (event && event.type && event.type.value) {
            switch (event.type.value) {
                case 'IPv4':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
                    )]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-MD5':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(32), Validators.maxLength(32)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-SHA1':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(40), Validators.maxLength(40)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'FileHash-SHA256':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(64), Validators.maxLength(64)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'URL':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Email':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Domain':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
            }

        }
    }

    indicatorTypeOptionsForFile(iocIndex, event) {
        const collectionControl = this.collectionIoCForm.get('indicators')['controls'];
        if (event && event.type && event.type.value) {

            switch (event.type.value) {
                case 'IPv4':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
                    )]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-MD5':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(32), Validators.maxLength(32)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-SHA1':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(40), Validators.maxLength(40)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'FileHash-SHA256':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(64), Validators.maxLength(64)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'URL':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Email':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Domain':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
            }
        }
    }

    hasError(errorType, event) {
        const collectionControl = this.collectionIoCForm.get('indicators')['controls'];
        if (event && event.type && event.type.value) {

            switch (event.type.value) {
                case 'IPv4':
                    if (errorType === 'required') {
                        return 'IP is required';
                    } else if (errorType === 'pattern') {
                        return 'IP Address is incorrect';
                    }
                    break;
                case 'FileHash-MD5':
                    if (errorType === 'required') {
                        return 'FileHash-MD5 is required';
                    } else if (errorType === 'pattern') {
                        return 'FileHash-MD5  is incorrect';
                    } else if (errorType === 'minlength') {
                        return 'FileHash-MD5 can only be 32 Characters';
                    } else if (errorType === 'maxlength') {
                        return 'FileHash-MD5 can only be 32 Characters';
                    }
                    break;
                case 'FileHash-SHA1':
                    if (errorType === 'required') {
                        return 'FileHash-SHA1 is required';
                    } else if (errorType === 'pattern') {
                        return 'FileHash-SHA1  is incorrect';
                    } else if (errorType === 'minlength') {
                        return 'FileHash-SHA1 can only be 40 Characters';
                    } else if (errorType === 'maxlength') {
                        return 'FileHash-SHA1 can only be 40 Characters';
                    }
                    break;

                case 'FileHash-SHA256':
                    if (errorType === 'required') {
                        return 'FileHash-SHA256 is required';
                    } else if (errorType === 'pattern') {
                        return 'FileHash-SHA256  is incorrect';
                    } else if (errorType === 'minlength') {
                        return 'FileHash-SHA256 can only be 64 Characters';
                    } else if (errorType === 'maxlength') {
                        return 'FileHash-SHA256 can only be 64 Characters';
                    }
                    break;

                case 'URL':
                    if (errorType === 'required') {
                        return 'URL is required';
                    } else if (errorType === 'pattern') {
                        return 'URL is incorrect';
                    }
                    break;

                case 'Email':
                    if (errorType === 'required') {
                        return 'Email is required';
                    } else if (errorType === 'pattern') {
                        return 'Email is incorrect';
                    }
                    break;

                case 'Domain':
                    if (errorType === 'required') {
                        return 'Domain is required';
                    }
                    break;
                case 'Email':
                    if (errorType === 'required') {
                        return 'Email is required';
                    }
                    break;

            }
        }
    }

    removeIndicator(iocIndex: number) {
        const updateCollectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        updateCollectionControl.removeAt(iocIndex);
    }

    get updateCollectionError() {
        return this.collectionIoCForm.controls;
    }

    add(event: any): void {
        const input = event.input;
        let value = event.value;

        // Add our tag
        if ((value || '').trim()) {
            value = value.trim();
            if (this.tags.find((test) => test.name.toLowerCase() === value.toLowerCase()) === undefined) {
                this.tags.push({name: value.trim()});
                this.collectionIoCForm.value.tags = this.tags;
            }

        }


        // Reset the input value
        if (input) {
            input.value = '';
        }
    }

    remove(event: any): void {
        const index = this.tags.findIndex(x => x.name === event.value);

        if (index >= 0) {
            this.tags.splice(index, 1);
            this.collectionIoCForm.value.tags = this.tags;
        }
    }

    openCollectionsDialog(value) {
        this.ref = this.dialogService.open(CommunityCollectionsComponent, {
            header: 'Submitted Collection Details',
            width: '90%',
            contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data:  {type: value, modal: 'create'}
        });
    }

    ngOnDestroy(): void {
        if (this.ref) {
            this.ref.close();
        }
    }
}

