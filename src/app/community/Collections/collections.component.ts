import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {Table} from 'primeng/table';
import {ConfirmationService} from 'primeng/api';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityService} from '../../core-services/community.service';
import {CommunityCollectionsComponent} from '../community-collections/community-collections.component';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';
import {CommunityIocsComponent} from '../community-iocs/community-iocs.component';
@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss'],
  providers: [ConfirmationService, DialogService, DynamicDialogRef]
})

export class CollectionsComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table | undefined;
  counter: any;
  indicatorData: any;
  error: any;
  ref: DynamicDialogRef;
  tagRef: DynamicDialogRef;
  colRef: DynamicDialogRef;
  deleteLoader: any = false;
  response: any;
  active: any;
  totalRecords: any;
  userRole: any;
  selectedAttackType: any;
  attackType = [
    {label: 'Collections', value: 'Collections'},
    {label: 'IPv4', value: 'IPv4'},
    {label: 'Malware', value: 'Malware'},
    {label: 'FileHash-MD5', value: 'FileHash-MD5'},
    {label: 'FileHash-SHA1', value: 'FileHash-SHA1'},
    {label: 'FileHash-SHA256', value: 'FileHash-SHA256'},
    {label: 'Domain', value: 'Domain'},
    {label: 'URL', value: 'URL'},
    {label: 'Email', value: 'Email'},
    {label: 'Tags', value: 'Tags'},
  ];
  tag: any;
  constructor(private communityservice: CommunityService, private toastr: ToastrService, private router: Router,
              private confirmationService: ConfirmationService, public dialogService: DialogService,
              private spinner: NgxSpinnerService, private route: ActivatedRoute) {
    this.userRole = sessionStorage.getItem('role');
    this.counter = 0;
    this.selectedAttackType = this.attackType[0].value;
    this.route.queryParams.subscribe(params => {
      if (params.tag) {
        this.tag = params.tag;
      }
    });
    this.getCollection();

  }
  dopdownAttackType(event) {
    this.counter = 0;
    console.log(this.selectedAttackType, event);
    this.getCollection();
  }

  ngOnInit(): void {
  }
  pagination(event) {
    this.counter  = event.first / 10;
    console.log(this.counter);
    this.getCollection();
  }
  getCollection() {
    const role = sessionStorage.getItem('role');
    this.spinner.show('collection-spinner');
    if (this.selectedAttackType === 'Collections') {
      this.communityservice.communityindicatorsALLindicators(this.counter, this.selectedAttackType, this.tag).subscribe(
          res => {
            this.spinner.hide('collection-spinner');
            this.indicatorData = res;
            if (this.indicatorData && this.indicatorData.collectionResponses) {
              this.totalRecords = this.indicatorData.totalElements;
              this.indicatorData = this.indicatorData.collectionResponses;
              for (let i = 0; i < this.indicatorData.length; i++) {
                if (this.indicatorData[i].indicatorCounts) {
                  this.indicatorData[i].indicatorCounts = this.indicatorData[i].indicatorCounts.totalCount;
                } else {
                  this.indicatorData[i].indicatorCounts = 1;
                }
              }
            }
          }, err => {
            this.spinner.hide('collection-spinner');

            this.error = err;
            this.toastr.error('Oops some problem occurred. Please wait');
          }
      );
    }
     else if (this.selectedAttackType !== 'Collections' && this.selectedAttackType !== 'Tags') {
      this.communityservice.communityindicatorsALLindicators(this.counter, this.selectedAttackType, null).subscribe(
          res => {
            this.spinner.hide('collection-spinner');
            this.indicatorData = res;
            if (this.indicatorData && this.indicatorData.summaryData) {
              this.totalRecords = this.indicatorData.totalElements;
              this.indicatorData = this.indicatorData.summaryData;
            }
          }, err => {
            this.spinner.hide('collection-spinner');

            this.error = err;
            this.toastr.error('Oops some problem occurred. Please wait');
          }
      );
    }
     else if (this.selectedAttackType === 'Tags') {
      this.communityservice.communityTagList().subscribe(
          res => {
            this.spinner.hide('collection-spinner');
            this.indicatorData = res;
          }, err => {
            this.spinner.hide('collection-spinner');
            this.error = err;
            this.toastr.error('Oops some problem occurred. Please wait');
          }
      );
    }

  }

  openCollectionsDialog(value) {
    this.colRef = this.dialogService.open(CommunityCollectionsComponent, {
      header: 'Collections',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data: {type: value, modal: 'list'}
    });
  }

  ngOnDestroy(): void {
    if (this.ref) {
      this.ref.close();
    }
    if (this.colRef) {
      this.colRef.close();
    }
    if (this.tagRef) {
      this.tagRef.close();
    }
  }

  updateAdvisory(id) {
    this.router.navigate(['community/updateCollections'], {queryParams: {id}});
  }

  deleteCollection(id) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this Collection?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.spinner.show('collection-spinner');

        this.communityservice.communityIndicatorDelete(id).subscribe(
            res => {
              this.spinner.hide('collection-spinner');
              this.toastr.success('Collection Deleted successfully');
              this.getCollection();
            },
            err => {
              this.spinner.hide('collection-spinner');
              this.error = err;
              this.toastr.error('Oops some problem occurred. Please wait');
            }
        );
      },
      reject: () => {
      }
    });

  }

  indicatorClick(indicator, tagName) {
    this.router.navigate(['community/explore/type'], {queryParams: {id: indicator, tag: tagName}});
  }

  showLinks() {
    const role = sessionStorage.getItem('role');
    if (role === 'ROLE_SOC_ADMIN' || role === 'ROLE_L1_ANALYST' || role === 'ROLE_L2_ANALYST' || role === 'ROLE_CLIENT_ADMIN' || role === 'ROLE_CLIENT_ANALYST') {
      return true;
    } else {
      this.router.navigate(['community/dashboard']);
    }
  }

  tagDetails(tag) {
    this.tagRef = this.dialogService.open(CommunityTagsComponent, {
      header: 'Tag Appearance',
      width: '80%',
      contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: tag}
    });
  }

  openDialog(value) {
    this.ref = this.dialogService.open(CommunityIocsComponent, {
      header: 'Indicator Appearance',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: this.selectedAttackType, value}
    });
  }

  userDetails(id) {
    sessionStorage.setItem('titleDetails', id);

  }
}
