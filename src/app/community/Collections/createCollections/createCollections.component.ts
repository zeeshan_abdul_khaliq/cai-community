import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {DatePipe} from '@angular/common';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityService} from '../../../core-services/community.service';
import {CommunityCollectionsComponent} from '../../community-collections/community-collections.component';

@Component({
    selector: 'app-createCollections',
    templateUrl: './createCollections.component.html',
    styleUrls: ['./createCollections.component.scss'],
    providers: [DialogService, DynamicDialogRef]
})

export class CreateCollectionsComponent implements OnInit {
    collectionIoCForm: FormGroup;
    mawlareUploadForm: FormGroup;
    IoCFileUploadForm: FormGroup;
    @ViewChild('file') malwareFile;
    malwarefilesToUpload: Set<File> = new Set();
    @ViewChild('iocfile') iocFile;
    iocFileToUpload: Set<File> = new Set();
    fortag: any;
    tags: any;
    descriptionAdvisory: any;
    activeIndex: any = 0;
    success: any;
    error: any;
    date: any;
    minDate: any;
    ref: DynamicDialogRef;
    tagsrequired: any = false;
    fileSubmit = false;

    indicatorsName: any[] = [
        {label: 'IPv4', value: 'IPv4'},
        {label: 'URL', value: 'URL'},
        {label: 'FileHash-MD5', value: 'FileHash-MD5'},
        {label: 'FileHash-SHA1', value: 'FileHash-SHA1'},
        {label: 'FileHash-SHA256', value: 'FileHash-SHA256'},
        {label: 'Email', value: 'Email'},
        {label: 'Domain', value: 'Domain'}
    ];
    selectedIndicator: any;

    constructor(private communityService: CommunityService, private router: Router, private toastr: ToastrService, public dialogService: DialogService,
                private spinner: NgxSpinnerService, private datePipe: DatePipe, private formBuilder: FormBuilder) {
        this.fileSubmit = false;
        this.tags = [];
        this.date = new Date();
        this.minDate = new Date();
        this.selectedIndicator = this.indicatorsName[0].value;
        this.collectionIoCForm = this.formBuilder.group({
            main: ['', [Validators.required, Validators.pattern('^[ A-Za-z0-9_.%,@#&$:_-]+$')]],
            description: ['', [Validators.required]],
            tags: ['', [Validators.required]],
            indicators: this.formBuilder.array([this.indicator(this.selectedIndicator, '')])
        });
        this.mawlareUploadForm = this.formBuilder.group({
            main: ['', [Validators.required, Validators.pattern('^[ A-Za-z0-9_.%,@#&$:_-]+$')]],
            description: ['', [Validators.required]],
            tags: ['', [Validators.required]],
            file: ['', [Validators.required]],
        });
        this.IoCFileUploadForm = this.formBuilder.group({
            main: ['', [Validators.required, Validators.pattern('^[ A-Za-z0-9_.%,@#&$:_-]+$')]],
            description: ['', [Validators.required]],
            tags: ['', [Validators.required]],
            file: ['', [Validators.required]],
        });

        // For Collection IoC Form dynamic Validaiton
        const collectionControl = this.collectionIoCForm.get('indicators')['controls'];
        collectionControl[0].controls.value.setValidators([Validators.required, Validators.pattern(
            '^(?!0\.0\.0\.0/)(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/\d+)?$'
        )]);
        collectionControl[0].controls.value.updateValueAndValidity();
    }

    ngOnInit(): void {
    }

    oncollectionIoCFormSubmit() {
        const collectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        for (let collectionIndex = 0; collectionIndex < collectionControl.controls.length; collectionIndex++){
            if (collectionControl.controls[collectionIndex].status === 'INVALID' ||
                collectionControl.controls[collectionIndex].value.value === '') {
                collectionControl.removeAt(collectionIndex);
            }
        }
        if (this.collectionIoCForm.invalid) {
            this.toastr.error('Please complete the form');
            return;
        }
        const event = this.collectionIoCForm.value;
        this.fortag = [];
        if (this.tags && this.tags.length === 0) {
            this.tagsrequired = true;
        } else if (this.tags && this.tags.length !== 0) {
            this.tagsrequired = false;
            for (let tagIndex = 0; tagIndex < this.tags.length; tagIndex++) {
                this.fortag.push({name: this.tags[tagIndex].name});
            }
        }
        const username = sessionStorage.getItem('username');
        this.spinner.show('createCollection-spinner');
        this.descriptionAdvisory = this.stripHtml(event.description);
        if (!this.descriptionAdvisory) {
            this.descriptionAdvisory = null;
        }
        const data = {
            description: this.descriptionAdvisory,
            indicators: event.indicators,
            tags: this.fortag,
            title: event.main,
            email: username
        };
        this.communityService.communityIndicatorPost(data).subscribe(res => {
                this.success = res;
                if (this.success.statusCode === 201 || this.success.statusCode === 200) {
                    this.spinner.hide('createCollection-spinner');
                    this.success = res;
                    this.tags = [];
                    this.toastr.success('Collection Created Successfully');
                    const id = this.success.id;
                    if (id) {
                        this.openCollectionsDialog(id);
                    }
                }
            },
            err => {
                this.spinner.hide('createCollection-spinner');
                this.error = err;
                if (this.error.status === 417) {
                    this.toastr.error('Failed to Insert the Collection => Reason: ' + this.error.error.msg);
                }else if (this.error.status === 409) {
                    this.toastr.error('Collection Already Exist' + this.error.error.msg);
                }else if (this.error.status === 400) {
                    this.toastr.error(this.error.error.msg);
                }else {
                    this.toastr.error('Oops some problem occurred. Please wait');

                }
            });


    }

    onmawlareUploadFormSubmit(files: Set<File>) {
        if (this.mawlareUploadForm.invalid) {
            this.toastr.error('Please complete the form');
            return;
        }
        const event = this.mawlareUploadForm.value;
        this.fortag = [];
        if (this.tags && this.tags.length === 0) {
            this.tagsrequired = true;
        } else if (this.tags && this.tags.length !== 0) {
            this.tagsrequired = false;
            for (let tagIndex = 0; tagIndex < this.tags.length; tagIndex++) {
                this.fortag.push({name: this.tags[tagIndex].name});
            }
        }
        const username = sessionStorage.getItem('username');
        this.spinner.show('createCollection-spinner');
        this.descriptionAdvisory = this.stripHtml(event.description);
        if (!this.descriptionAdvisory) {
            this.descriptionAdvisory = null;
        }
        const data = {
            description: this.descriptionAdvisory,
            tags: this.fortag,
            title: event.main,
        };
        files.forEach(file => {
            const formData: FormData = new FormData();
            formData.append('file', file, file.name);
            this.communityService.communityIndicatorMalware(formData, data).subscribe(res => {
                    this.success = res;
                    if (this.success.statusCode === 201 || this.success.statusCode === 200) {
                        this.spinner.hide('createCollection-spinner');
                        this.success = res;
                        this.tags = [];
                        this.toastr.success('Malware Uploaded Successfully');
                        const id = this.success.id;
                        if (id) {
                            this.openCollectionsDialog(id);
                        }
                        this.malwareFile.nativeElement.value = '';
                        files.clear();
                        this.malwarefilesToUpload.clear();
                    }
                },
                err => {
                    this.spinner.hide('createCollection-spinner');
                    this.error = err;
                    if (this.error.status === 417) {
                        this.toastr.error('Failed to Upload the malware => Reason: ' + this.error.error.msg);
                    }else if (this.error.status === 409) {
                        this.toastr.error('Malware Already Exist' + this.error.error.msg);
                    }else if (this.error.status === 400) {
                        this.toastr.error(this.error.error.msg);
                    }else {
                        this.toastr.error('Oops some problem occurred. Please wait');

                    }
                });
        });

    }

    onIoCFileUploadFormSubmit(files: Set<File>) {
        const collectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        for (let collectionIndex = 0; collectionIndex < collectionControl.controls.length; collectionIndex++){
            if (collectionControl.controls[collectionIndex].status === 'INVALID' || collectionControl.controls[collectionIndex].value.value === '') {
                collectionControl.removeAt(collectionIndex);
            }
        }

        if (this.IoCFileUploadForm.invalid) {
            this.toastr.error('Please complete the form');
            return;
        }
        const event = this.IoCFileUploadForm.value;
        this.fortag = [];
        if (this.tags && this.tags.length === 0) {
            this.tagsrequired = true;
        } else if (this.tags && this.tags.length !== 0) {
            this.tagsrequired = false;
            for (let tagIndex = 0; tagIndex < this.tags.length; tagIndex++) {
                this.fortag.push({name: this.tags[tagIndex].name});
            }
        }
        const username = sessionStorage.getItem('username');
        this.spinner.show('createCollection-spinner');
        this.descriptionAdvisory = this.stripHtml(event.description);
        if (!this.descriptionAdvisory) {
            this.descriptionAdvisory = null;
        }
        const data = {
            description: this.descriptionAdvisory,
            tags: this.fortag,
            title: event.main,
        };
        files.forEach(file => {
            const formData: FormData = new FormData();
            formData.append('file', file, file.name);
            this.communityService.communityIndicatorIOC(formData, data).subscribe(res => {
                    this.success = res;
                    if (this.success && this.success.length > 0) {
                        this.spinner.hide('createCollection-spinner');
                        this.success = res;
                        this.fileSubmit = true;

                        this.toastr.success('Ioc File Uploaded Successfully');
                        this.uploadIndicatorsFromFile(this.success, data);
                        this.iocFile.nativeElement.value = '';
                        files.clear();
                        this.iocFileToUpload.clear();
                    }
                },
                err => {
                    this.fileSubmit = false;
                    this.spinner.hide('createCollection-spinner');
                    this.error = err;
                    if (this.error.status === 417) {
                        this.toastr.error('Failed to Upload the IoC File => Reason: ' + this.error.error.msg);
                    }else if (this.error.status === 409) {
                        this.toastr.error( this.error.error.msg);
                    }else if (this.error.status === 400) {
                        this.error = this.error.error.typeErrors;
                        for (const err of this.error) {
                            this.toastr.error(err.error);
                        }
                    }else if (this.error.status === 400) {
                        this.toastr.error(this.error.error.msg);
                    } else {
                        this.toastr.error('Oops some problem occurred. Please wait');

                    }
                });
        });

    }

    uploadIndicatorsFromFile(iocs, data) {
        this.activeIndex = 1;
        this.fortag = [];
        this.tags = data.tags;

        if (data.tags.length > 0) {
            for (const tag of data.tags) {
                this.fortag.push(tag.name);
            }
        }
        this.collectionIoCForm.patchValue({
            main: data.title,
            description: data.description,
            tags: this.fortag,
        });
        const collectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        let count = collectionControl.length;
        if (count > 1) {
            collectionControl.removeAt(count);
            collectionControl[count] = [];
        } else {
            collectionControl.removeAt(0);
            collectionControl[0] = [];
            count = 0;
        }
        for (let iocIndex = 0; iocIndex < iocs.length; iocIndex++) {
            const edd = iocs[iocIndex];
            if ((edd.type !== null || edd.type !== undefined) && (edd.value !== null || edd.value !== undefined)) {
                collectionControl.push(this.setindicator(edd.type, edd.value));
                this.indicatorTypeOptionsForFile(count, edd.type);
                count++;
            }
        }
    }
    getSelectedHeader(event) {
        this.activeIndex = event.index;
        if (this.activeIndex === 0) {
            this.fileSubmit = false;
            this.fortag = [];
            this.collectionIoCForm.patchValue({
                main: '',
                description: '',
                tags: this.fortag,
            });
            this.IoCFileUploadForm.patchValue({
                main: '',
                description: '',
                tags: '',
                file: ''
            });
            this.IoCFileUploadForm.reset();
            const collectionControl = this.collectionIoCForm.controls.indicators as FormArray;
            const count = collectionControl.length;
            collectionControl.clear();
            this.selectedIndicator = 'IPv4';
            collectionControl.push(this.indicator(this.selectedIndicator, ''));
        }

    }
    stripHtml(html) {
        const temporalDivElement = document.createElement('div');
        temporalDivElement.innerHTML = html;
        return temporalDivElement.textContent || temporalDivElement.innerText || '';
    }

    cancel() {
        this.router.navigate(['/community/collections']);
    }

    onMalwareFileAdded() {
        this.malwarefilesToUpload.clear();
        const files: { [key: string]: File } = this.malwareFile.nativeElement.files;
        for (const key in files) {
            if (!isNaN(parseInt(key))) {
                this.malwarefilesToUpload.add(files[key]);
            }
        }
    }

    oniocFileToUploadAdded() {
        this.iocFileToUpload.clear();
        const files: { [key: string]: File } = this.iocFile.nativeElement.files;
        for (const key in files) {
            if (!isNaN(parseInt(key))) {
                this.iocFileToUpload.add(files[key]);
            }
        }
    }

    indicator(type, value): FormGroup {
        return this.formBuilder.group({
            type: [type, Validators.required],
            value: [value, [Validators.required, Validators.pattern(
                '^(?!0\.0\.0\.0/)(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/\d+)?$'
            )]],
        });
    }

    setindicator(type, value): FormGroup {
        return this.formBuilder.group({
            type: [type, Validators.required],
            value: [value, [Validators.required]],
        });
    }
    addIndicator(index) {
        const collectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        let checkStatus = 0;
        for (const collectionValue of collectionControl.controls){
            if (collectionValue.status === 'INVALID' || collectionValue.value.value === '') {
                checkStatus++;
            }
        }
        if (checkStatus < 2) {
            collectionControl.push(this.indicator(collectionControl.value[index].type, ''));
        }
    }

    getPlaceholder(iocIndex, event) {
        if (event && event.type && event.type.value) {
            switch (event.type.value) {
                case 'IPv4':
                    return 'Enter IP (192.16.0.12)';
                case 'FileHash-MD5':
                    return 'Enter Hash Md5 ';

                case 'FileHash-SHA1':
                    return 'Enter Hash SHA1 ';

                case 'FileHash-SHA256':
                    return 'Enter Hash SHA256 ';

                case 'URL':
                    return 'Enter URL e.g www.google.com';


                case 'Email':
                    return 'Enter Email (example@abc.com)';
                case 'Domain':
                    return 'Enter Domain e.g www.google.com';

            }
        }
    }

    indicatorTypeOptions(iocIndex, event) {

        const collectionControl = this.collectionIoCForm.get('indicators')['controls'];
        if (event && event.type && event.type.value) {
            switch (event.type.value) {
                case 'IPv4':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
                    )]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-MD5':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(32), Validators.maxLength(32)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-SHA1':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(40), Validators.maxLength(40)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'FileHash-SHA256':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(64), Validators.maxLength(64)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'URL':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Email':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Domain':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
            }
        }
    }

    indicatorTypeOptionsForFile(iocIndex, event) {
        const collectionControl = this.collectionIoCForm.get('indicators')['controls'];
        if (event && event.type && event.type.value) {
            switch (event.type.value) {
                case 'IPv4':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
                    )]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-MD5':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(32), Validators.maxLength(32)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
                case 'FileHash-SHA1':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(40), Validators.maxLength(40)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'FileHash-SHA256':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9]+$'), Validators.minLength(64), Validators.maxLength(64)]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'URL':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Email':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required, Validators.pattern(
                        '^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;

                case 'Domain':
                    collectionControl[iocIndex].controls.value.setValidators([Validators.required]);
                    collectionControl[iocIndex].controls.value.updateValueAndValidity();
                    break;
            }
        }
    }

    hasError(errorType, event) {
        if (event && event.type && event.type.value) {
            switch (event.type.value) {
                case 'IPv4':
                    if (errorType === 'required') {
                        return 'IP is required';
                    } else if (errorType === 'pattern') {
                        return 'IP Address is incorrect';
                    }
                    break;
                case 'FileHash-MD5':
                    if (errorType === 'required') {
                        return 'FileHash-MD5 is required';
                    } else if (errorType === 'pattern') {
                        console.log(errorType);
                        return 'FileHash-MD5  is incorrect';
                    } else if (errorType === 'minlength') {
                        return 'FileHash-MD5 can only be 32 Characters';
                    } else if (errorType === 'maxlength') {
                        return 'FileHash-MD5 can only be 32 Characters';
                    }
                    break;
                case 'FileHash-SHA1':
                    if (errorType === 'required') {
                        return 'FileHash-SHA1 is required';
                    } else if (errorType === 'pattern') {
                        return 'FileHash-SHA1  is incorrect';
                    } else if (errorType === 'minlength') {
                        return 'FileHash-SHA1 can only be 40 Characters';
                    } else if (errorType === 'maxlength') {
                        return 'FileHash-SHA1 can only be 40 Characters';
                    }
                    break;

                case 'FileHash-SHA256':
                    if (errorType === 'required') {
                        return 'FileHash-SHA256 is required';
                    } else if (errorType === 'pattern') {
                        return 'FileHash-SHA256  is incorrect';
                    } else if (errorType === 'minlength') {
                        return 'FileHash-SHA256 can only be 64 Characters';
                    } else if (errorType === 'maxlength') {
                        return 'FileHash-SHA256 can only be 64 Characters';
                    }
                    break;

                case 'URL':
                    if (errorType === 'required') {
                        return 'URL is required';
                    } else if (errorType === 'pattern') {
                        return 'URL is incorrect';
                    }
                    break;

                case 'Email':
                    if (errorType === 'required') {
                        return 'Email is required';
                    } else if (errorType === 'pattern') {
                        return 'Email is incorrect';
                    }
                    break;

                case 'Domain':
                    if (errorType === 'required') {
                        return 'Domain is required';
                    }
                    break;
                case 'Email':
                    if (errorType === 'required') {
                        return 'Email is required';
                    }
                    break;

            }
        }
    }

    removeIndicator(iocIndex: number) {
        const collectionControl = this.collectionIoCForm.controls.indicators as FormArray;
        collectionControl.removeAt(iocIndex);
    }

    downloadJSONsample() {
        const link = document.createElement('a');
        link.download = 'filename.json';
        link.href = '../assets/community/jsonSample.json';
        link.click();
    }

    get collectionError() {
        return this.collectionIoCForm.controls;
    }

    get malwareError() {
        return this.mawlareUploadForm.controls;
    }

    get iocerror() {
        return this.IoCFileUploadForm.controls;
    }

    add(event: any): void {
        const input = event.input;
        let value = event.value;

        // Add our tag
        if ((value || '').trim()) {
            value = value.trim();
            if (this.tags.find((test) => test.name.toLowerCase() === value.toLowerCase()) === undefined) {
                this.tags.push({name: value.trim()});
                this.collectionIoCForm.value.tags = this.tags;
            }

        }


        // Reset the input value
        if (input) {
            input.value = '';
        }
    }

    openCollectionsDialog(value) {
        this.ref = this.dialogService.open(CommunityCollectionsComponent, {
            header: 'Submitted Collection Details',
            width: '90%',
            contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value, modal: 'create'}
        });
    }
    remove(event: any): void {
        const index = this.tags.findIndex(x => x.name === event.value);

        if (index >= 0) {
            this.tags.splice(index, 1);
            this.collectionIoCForm.value.tags = this.tags;
        }
    }

    ngOnDestroy(): void {
        if (this.ref) {
            this.ref.close();
        }
    }
}

