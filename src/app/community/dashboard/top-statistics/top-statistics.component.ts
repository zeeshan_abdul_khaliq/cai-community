import { Component, OnInit } from '@angular/core';
import {EChartsOption} from 'echarts';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {CommunityService} from '../../../core-services/community.service';

@Component({
  selector: 'app-top-statistics',
  templateUrl: './top-statistics.component.html',
  styleUrls: ['./top-statistics.component.scss'],
  providers: [DialogService]
})
export class TopStatisticsComponent implements OnInit {

  spin: any = false;
  showTable = true;
  hideTable = false;
  data: any;
  counts: any;
  indicator: any;
  value: any;
  userDetail: any = false;
  display = 'none';
  taipChart: EChartsOption = {};
  ref: DynamicDialogRef;
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private toastr: ToastrService, public dialogService: DialogService,
              private communityService: CommunityService) { }

  ngOnInit() {
    this.spinner.show('taip-spinner');
    this.counts = [];
    this.indicator = [];
    this.value = [];
    this.communityService.communityIndicatorUTOPSTATS().subscribe(
        res => {
          this.spinner.hide('taip-spinner');
          this.data = res;
          if (this.data) {
            this.showTable = true;
            this.hideTable = false;
            for (let i = 0; i < this.data.length; i++) {
              this.indicator.push(this.data[i].type);
              this.counts.push(this.data[i].counts);
              const data = {
                value: this.data[i].counts,
                name: this.data[i].type,
              };
              this.value.push(data);
            }
            this.taipChart = {
              tooltip: {
                trigger: 'item',
                formatter(tooltipItem: any) {
                  const index = tooltipItem.dataIndex;
                  if (res[index].counts >= 1000 && res[index].counts <= 99999) {
                    res[index].counts = res[index].counts.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                  } else if (res[index].counts >= 100000 && res[index].counts <= 999999999) {
                    res[index].counts = (res[index].counts / 1000000);
                    res[index].counts = Math.round(res[index].counts * 100) / 100 + ' M';
                  } else if (res[index].counts >= 1000000000 && res[index].counts <= 999999999999) {
                    res[index].counts = (res[index].counts / 1000000000);
                    res[index].counts = Math.round(res[index].counts * 10) / 10 + ' B';
                  }
                  return 'Indicator: ' + res[index].type + '<br/>' + 'Count: ' + res[index].counts;
                },
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                padding: 10,
                position: [30, 50],
              },

              grid: {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
              },
              color: [
                '#68CCE3', '#33406C', '#FFBA53', '#37CE6A'
              ],
              legend: {
                left: 'left',
                data: this.indicator,

              },
              series: [
                {
                  type: 'pie',
                  radius: ['50%', '70%'],
                  data: this.value,
                  label: {
                    position: 'outer',
                    alignTo: 'labelLine',
                    bleedMargin: 5
                  },
                  labelLine: {
                    smooth: 0.2,
                    length: 3,
                    length2: 3
                  }
                }
              ],
            };


          } else {
            this.showTable = false;
            this.hideTable = true;
          }

        }, err => {
          this.spinner.hide('taip-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');

        });


  }


  sendData(event) {
    const data = event.data.name;
    const index = event.dataIndex;
  }

  routeToExplore(event) {
    const data = event.data.name;
    this.router.navigate(['/community/explore'], {queryParams: {type: data}});
  }
}
