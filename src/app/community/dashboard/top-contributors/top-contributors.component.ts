import { Component, OnInit } from '@angular/core';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {CommunityService} from '../../../core-services/community.service';

@Component({
  selector: 'app-top-contributors',
  templateUrl: './top-contributors.component.html',
  styleUrls: ['./top-contributors.component.scss'],
  providers: [DialogService]
})
export class TopContributorsComponent implements OnInit {

  showTable = true;
  hideTable = false;
  listOfData: any;
  error: any;
  ref: DynamicDialogRef;
  constructor(private spinner: NgxSpinnerService,
              private toastr: ToastrService, public dialogService: DialogService,
              private communityService: CommunityService, private router: Router) {
  }

  ngOnInit(): void {
    this.spinner.show('ti-spinner');
    this.communityService.communityTopContributors().subscribe(
        res => {
          this.spinner.hide('ti-spinner');
          this.listOfData = res;
          if (this.listOfData) {
            this.showTable = true;
            this.hideTable = false;
          } else {
            this.showTable = false;
            this.hideTable = true;
          }
        }, err => {
          this.spinner.hide('ti-spinner');
          this.error = err;
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');

        }
    );
  }

  openContributorsDialog(email, cont) {
    this.router.navigate(['/community/contributor'], { queryParams: {email, contributor: cont}, skipLocationChange: true});
  }

}
