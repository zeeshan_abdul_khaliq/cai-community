import {Component, OnDestroy, OnInit} from '@angular/core';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {EChartsOption} from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {CommunityService} from '../../../core-services/community.service';
import {CommunityTagsComponent} from '../../community-tags/community-tags.component';

@Component({
  selector: 'app-top-tags',
  templateUrl: './top-tags.component.html',
  styleUrls: ['./top-tags.component.scss'],
  providers: [DialogService, DynamicDialogRef]
})
export class TopTagsComponent implements OnInit, OnDestroy {

  tspChart: EChartsOption = {};
  response: any;
  counts: any;
  indicator: any;
  value: any;
  error: any;
  dataTypes: any;
  showTable = true;
  hideTable = false;
  navigate = false;
  navigateIoc = false;
  navigatehash = false;
  navigateViewAll = false;
  navigateIocD = false;
  ref: DynamicDialogRef;
  constructor(private spinner: NgxSpinnerService, private dialogRef: DynamicDialogRef,
              private toastr: ToastrService, public dialogService: DialogService,
              private communityService: CommunityService) { }

  ngOnInit(): void {
    this.sectorBaseIoc();
  }
  sectorBaseIoc() {
    this.indicator = [];
    this.counts = [];
    this.navigate = false;
    this.navigateIoc = false;
    this.navigatehash = false;
    this.navigateViewAll = false;
    this.navigateIocD = false;

    this.spinner.show('sbi-spinner');
    this.communityService.communityIndicatorTopTAGS().subscribe(
        res => {
          this.spinner.hide('sbi-spinner');
          this.response = res;
          if (this.response) {
            this.showTable = true;
            this.hideTable = false;
            for (let i = 0; i < this.response.length; i++) {
              this.indicator.push(this.response[i].tag);
              this.counts.push(this.response[i].counts);
            }
            this.createSBIChart(res);
          } else {
            this.showTable = false;
            this.hideTable = true;

          }
        }, err => {
          this.spinner.hide('sbi-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');
        });
  }

  sbiDetails(itemIndex) {
    const l = itemIndex.name;
    this.navigateIoc = true;
    this.navigatehash = false;
    if (itemIndex.name === 'Hash') {
      this.navigateViewAll = false;
    } else {
      this.navigateViewAll = true;
      this.dataTypes = itemIndex.name;
    }
  }

  createSBIChart(res) {
    this.tspChart = {
      tooltip: {
        show: true,
        trigger: 'axis',
        formatter(tooltipItem) {
          let relVal = tooltipItem[0].name;
          relVal += tooltipItem[0].dataIndex;
          const index = tooltipItem[0].dataIndex;
          if (res[index].counts >= 1000 && res[index].counts <= 99999) {
            res[index].counts = res[index].counts.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          } else if (res[index].counts >= 100000 && res[index].counts <= 999999999) {
            res[index].counts = (res[index].counts / 1000000);
            res[index].counts = Math.round(res[index].counts * 100) / 100 + ' M';
          } else if (res[index].counts >= 1000000000 && res[index].counts <= 999999999999) {
            res[index].counts = (res[index].counts / 1000000000);
            res[index].counts = Math.round(res[index].counts * 10) / 10 + ' B';
          }
          return 'Indicator: ' + res[index].tag + '<br/>' + 'Count: ' + res[index].counts;
        },
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        borderColor: '#aaa',
        showContent: true,
        borderWidth: 1,
        padding: 10,
        axisPointer: {
          type: 'none'
        }
      },
      grid: {
        left: 45,
        top: 10,
        right: 0,
        bottom: 40
      },
      xAxis: {
        data: this.indicator,

        axisLabel: {
          rotate: 30
        },
        axisTick: {
          alignWithLabel: false
        },
        splitLine: {
          show: false
        }
      },
      yAxis: {
        type: 'value',

      },
      series: [{
        name: 'Attacking IOC',
        type: 'bar',
        barMaxWidth: 60,
        data: this.counts,
        color: '#33406C'

      }],
    };

  }

  openDialog(value) {
    let type;
    this.response.map((item, i) => {
      if (i === value.dataIndex)
      {
        type = item.tag;
        return item;
      }
    });
    this.ref = this.dialogService.open(CommunityTagsComponent, {
      header: 'Tag Appearance',
      width: '80%',
      contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type}
    });
  }

  ngOnDestroy(): void {
    if (this.ref) {
      this.ref.close();
    }
  }
}
