import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentCollectionsComponent } from './recent-collections.component';

describe('RecentCollectionsComponent', () => {
  let component: RecentCollectionsComponent;
  let fixture: ComponentFixture<RecentCollectionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentCollectionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentCollectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
