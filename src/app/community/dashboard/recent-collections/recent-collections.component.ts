import {Component, OnDestroy, OnInit} from '@angular/core';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {CommunityService} from '../../../core-services/community.service';
import {CommunityCollectionsComponent} from '../../community-collections/community-collections.component';
@Component({
  selector: 'app-recent-collections',
  templateUrl: './recent-collections.component.html',
  styleUrls: ['./recent-collections.component.scss'],
  providers: [DialogService, DynamicDialogRef]
})
export class RecentCollectionsComponent implements OnInit, OnDestroy {

  showTable = true;
  hideTable = false;
  listOfData: any;
  error: any;
  ref: DynamicDialogRef;
  constructor(private spinner: NgxSpinnerService,
              private toastr: ToastrService, public dialogService: DialogService,
              private communityService: CommunityService) {
  }

  ngOnInit(): void {
    this.spinner.show('ti-spinner');
    this.communityService.communityIndicatorRecentCollections().subscribe(
        res => {
          this.spinner.hide('ti-spinner');
          this.listOfData = res;
          if (this.listOfData) {
            this.showTable = true;
            this.hideTable = false;
          } else {
            this.showTable = false;
            this.hideTable = true;
          }
        }, err => {
          this.spinner.hide('ti-spinner');
          this.error = err;
          this.showTable = false;
          this.hideTable = true;
          if (this.error === 400) {
            this.toastr.error(this.error.error.message);
          } else {
            this.toastr.error('Oops some problem occurred. Please wait');
          }
        }
    );
  }
  openDialog(value) {
    this.ref = this.dialogService.open(CommunityCollectionsComponent, {
      header: 'Collection Details',
      width: '80%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: value, modal: 'list'}
    });
  }

  ngOnDestroy(): void {
    if (this.ref) {
      this.ref.close();
    }
  }

}
