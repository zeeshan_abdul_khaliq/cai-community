import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityTagsComponent } from './community-tags.component';

describe('CommunityTagsComponent', () => {
  let component: CommunityTagsComponent;
  let fixture: ComponentFixture<CommunityTagsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommunityTagsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
