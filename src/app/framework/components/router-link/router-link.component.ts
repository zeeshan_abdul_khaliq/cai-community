import { Component, OnInit, Input } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-router-link',
  templateUrl: './router-link.component.html',
  styleUrls: ['./router-link.component.scss']
})
export class RouterLinkComponent implements OnInit {

  @Input() router: string;
  @Input() label: string;
  @Input() icon: string;

  constructor() { }

  ngOnInit(): void {
  }

}
