import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

  @Input() type: string;
  @Input() placeholder: string;
  @Input() id: string;
  @Input() class: string;
  @Input() icon: string;
  @Input() disabled: boolean = false;
  @Input('isRequired') isRequired: boolean;

  inputForm: FormGroup;

  constructor(private fb: FormBuilder) {}
  
  ngOnInit() {
    this.inputForm = this.fb.group({
      inputName: ['', Validators.required],
    });
  }

}
