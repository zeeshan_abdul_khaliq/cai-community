import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {ToastrService} from 'ngx-toastr';
import {CommunityService} from '../../../core-services/community.service';
import {CalendarSettingService} from '../../../core-services/calendarSetting.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];
  role: string;
  checkIfdisabeldRoles = true;
  exploreItems: any[];
  exploreResponse: any;

  constructor(private communityService: CommunityService, private toastr: ToastrService,
              private calendarSettings: CalendarSettingService) {
    this.exploreItems = [];
    this.role = sessionStorage.getItem('role');
    if (this.role === 'Master Soc Analyst' || this.role === 'Distributor Soc Analyst' || this.role === 'Partner Soc Analyst') {
      this.checkIfdisabeldRoles = false;
    }
    this.items = [
      {
        label: 'Home',
        routerLink: '/cai/home',
      },
      {
        label: 'Threat Intelligence Platform',
        styleClass: 'modules',
          routerLink: '/tip/dashboard',
      },
      {
        label: 'Threat Sharing Community',
        styleClass: 'modules',
        items: [
            {
                label: 'Dashboard',
                routerLink: '/community/dashboard',
                routerLinkActiveOptions: true
            },

            {
                label: 'Advisory',
                routerLink: '/community/advisory',
                routerLinkActiveOptions: true
            },
            {
                label: 'Explore',
                routerLinkActiveOptions: true,
                items: [],
            },
            {
                label: 'Create Collection',
                routerLink: '/community/createCollections',
                routerLinkActiveOptions: true
            },

          ]

      },
        {
            label: 'Advanced Persistent Threats',
            styleClass: 'modules',
            routerLink: '/apt/dashboard',

        },
        {
            label: 'Infrastructure Monitoring',
            routerLink: '/networkMon/dashboard',
        },
        {
            label: 'Vulnerability Prioritization',
            styleClass: 'modules',
            routerLink: '/vp/dashboard',

        },
      {
        label: 'Distributed Deception Platform',
        styleClass: 'modules',
      },
      {
        label: 'SIEM',
        styleClass: 'modules',
      },
      {
        label: 'SOAR',
      },

      {
        label: 'Third Party Vendors',
        styleClass: 'modules',
        items: [
          {
            label: 'Skurio - DRP ',
            routerLink: '#',
          },
          {
            label: 'RiskXchange - ASM',
            routerLink: '#',
          }

        ]
      },

    ];
  }

  ngOnInit(): void {

    this.getExplore();

  }
  getExplore(): void {
    this.exploreItems = [];
    this.items[2].items[2].items = [];
    this.communityService.communityIndiactorExploreCount().subscribe(
        res => {
              this.exploreResponse = res;
              if (this.exploreResponse) {
                  for (const explore of this.exploreResponse) {
                      const item = {
                          label: explore.type + '  (' + this.calendarSettings.transformNumbers(explore.count) + ')',
                          routerLink: '/community/explore',
                          queryParams: {type: explore.type}
                      };
                      this.exploreItems.push(item);
                  }
                  this.items[2].items[2].items = this.exploreItems;
              }
          }, err => {
              this.toastr.error('Oops some problem occurred. Please wait');
          });
  }

}
