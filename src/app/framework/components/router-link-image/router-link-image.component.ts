import { Component, OnInit, Input } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-router-link-image',
  templateUrl: './router-link-image.component.html',
  styleUrls: ['./router-link-image.component.scss']
})
export class RouterLinkImageComponent implements OnInit {

  @Input() router: string;
  @Input() label: string;
  @Input() image: string;
  
  constructor() { }

  ngOnInit(): void {
  }

}
